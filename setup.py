#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import os
from distutils import log

import pkg_resources
import sys
from setuptools import find_packages, setup
from setuptools.command.install import install


class PostInstallConfigFiles(install):

    def run(self):

        # if we use do_egg_install() here, the bdist_wheel command will fail
        install.run(self)

        try:
            # install workloads library
            from appdirs import user_config_dir
            dest_dir = os.path.join(user_config_dir('benchmarking-suite'), 'workloads')
            log.info('Installing default stdlib workloads configuration into {0}'.format(dest_dir))
            os.makedirs(dest_dir, exist_ok=True)
            #files = pkg_resources.resource_listdir(pkg_resources.Requirement.parse("benchsuite.stdlib"), '/'.join(('benchsuite','stdlib', 'data', 'OLD_benchmarks')))
            files = os.listdir('data/workloads')
            for f in [file for file in files]:
                # content = pkg_resources.resource_string(pkg_resources.Requirement.parse("benchsuite.stdlib"),'/'.join(('benchsuite','stdlib', 'data', 'OLD_benchmarks', f)))
                with open('data/workloads/'+f, 'r') as myfile:
                    content = myfile.read()
                dest_file = os.path.join(dest_dir, f)
                log.info('Installing {0} in {1}'.format(f, dest_file))
                if os.path.isfile(dest_file):
                    log.debug('File {0} already exists. Renaming it to {1}'.format(dest_file, dest_file+'.bkp'))
                    os.rename(dest_file, dest_file + '.bkp')
                with open(dest_file, "w") as text_file:
                    text_file.write(content)

            # install providers
            dest_dir = os.path.join(user_config_dir('benchmarking-suite'), 'providers')
            log.info('Installing default stdlib providers configuration into {0}'.format(dest_dir))
            os.makedirs(dest_dir, exist_ok=True)
            #files = pkg_resources.resource_listdir(pkg_resources.Requirement.parse("benchsuite.stdlib"), '/'.join(('benchsuite','stdlib', 'data', 'providers')))
            files = os.listdir('data/providers')
            for f in [file for file in files]:
                #content = pkg_resources.resource_string(pkg_resources.Requirement.parse("benchsuite.stdlib"),'/'.join(('benchsuite','stdlib', 'data', 'providers', f)))
                with open('data/providers/' + f, 'r') as myfile:
                    content = myfile.read()
                dest_file = os.path.join(dest_dir, f)
                log.info('Installing {0} in {1}'.format(f, dest_file))
                if os.path.isfile(dest_file):
                    log.debug('File {0} already exists. Renaming it to {1}'.format(dest_file, dest_file+'.bkp'))
                    os.rename(dest_file, dest_file + '.bkp')
                with open(dest_file, "w") as text_file:
                    text_file.write(content)

        except pkg_resources.DistributionNotFound as ex:
            # this might be normal when invoked with bdist_wheel because the package is not installed yet..??
            #
            raise ex


if 'install' in sys.argv or 'bdist_wheel' in sys.argv:
    cmdclass = {'install': PostInstallConfigFiles}
else:
    cmdclass={}

setup(

    packages=find_packages('src'),
    namespace_packages=['benchsuite'],
    package_dir={'': 'src'},

    include_package_data=True,

    entry_points={
        'benchsuite.provider.controller': [
            'docker = benchsuite.stdlib.providers.docker.controller:DockerProviderController',
            'openstack = benchsuite.stdlib.providers.libcloud.controller:LibcloudProviderController',
            'aws-ec2 = benchsuite.stdlib.providers.libcloud.controller:LibcloudProviderController',
            'kubernetes = benchsuite.stdlib.providers.kubernetes.controller:KubernetesProviderController',
            'holo = benchsuite.stdlib.providers.holo:HoloProviderController'
        ],
        'benchsuite.provider.provisioner': [
            'openstack-docker = benchsuite.stdlib.provisioners.openstack_docker:OpenstackDockerProvisioner',
            'kubernetes = benchsuite.stdlib.provisioners.kubernetes_kubernetes:KubernetesKubernetesProvisioner',
            'docker = benchsuite.stdlib.provisioners.docker_docker:DockerDockerProvisioner',
            'openstack-vm = benchsuite.stdlib.provisioners.openstack_ssh:OpenstackSSHProvisioner',
            'holo-docker = benchsuite.stdlib.providers.holo:HoloProvisioner',
            'holo-ssh = benchsuite.stdlib.providers.holo:HoloSSHProvisioner'
        ],
        'benchsuite.workload.controller': [
            'jb = benchsuite.stdlib.workloads.jb.jb_controller:JBWorkloadController'
        ],
        'benchsuite.provider.loader': [
          'db = benchsuite.stdlib.loaders.provider:DBProviderLoader',
          'file = benchsuite.stdlib.loaders.provider:FileProviderLoader',
          'file:prop = benchsuite.stdlib.loaders.provider:FileProviderLoader'
        ],
        'benchsuite.workload.loader': [
          'db = benchsuite.stdlib.loaders.workload:DBWorkloadLoader',
          'file = benchsuite.stdlib.loaders.workload:FileWorkloadLoader'
        ],
        'benchsuite.profiler': [
          'prometheus-kubernetes = benchsuite.stdlib.profilers.dockers_prometheus:KubernetesPrometheusProfiler',
          'docker-stats = benchsuite.stdlib.profilers.docker_stats:DockerStatsProfiler',
          'k8s-docker-stats = benchsuite.stdlib.profilers.kubernetes_docker_stats:KubernetesDockerStats',
          'pledger-appprofiler = benchsuite.stdlib.profilers.pledger_appprofiler:PledgerAppProfilerProfiler'
        ],
        'benchsuite.workload': [
          'jb-workload = benchsuite.stdlib.workloads.jb.jb:JBWorkload'
        ],
        'benchsuite.parser': [
          'sysbench-cpu = benchsuite.stdlib.parsers.sysbench:SysbenchCPUParser',
          'ycsb = benchsuite.stdlib.parsers.ycsb:YCSBParser',
          'tfb = benchsuite.stdlib.parsers.tfb:TFBParser',
          'iperf = benchsuite.stdlib.parsers.iperf:IPerfParser',
          'generic-regex-parser = benchsuite.stdlib.parsers.common:GenericRegexParser'
        ],
        'benchsuite.jb.executors': [
          'kubernetes-container = benchsuite.stdlib.workloads.jb.executors.kubernetes:KubernetesContainerJobExecutor',
          'docker-container = benchsuite.stdlib.workloads.jb.executors.docker:DockerContainerJobExecutor',
          'ssh-shell = benchsuite.stdlib.workloads.jb.executors.shell:ShellJobExecutor'
        ]
    },

    install_requires=['kubernetes==12.0.1', 'paramiko==2.4.2', 'apache-libcloud==2.3.0', 'pyvcloud==23.0.0', 'benchsuite.core', 'sshtunnel', 'docker==4.4.4', 'pydantic','ciso8601','backoff==1.11.1','jsonpath-ng==1.5.3'],
    setup_requires=['appdirs'],
    cmdclass=cmdclass
)
