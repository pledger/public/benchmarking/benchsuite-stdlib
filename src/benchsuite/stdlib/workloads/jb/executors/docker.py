#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

from benchsuite.core.util.strings import random_id
from benchsuite.stdlib.workloads.jb.executors.common import ContainerJobExecutor

logger = logging.getLogger(__name__)


class DockerContainerJobExecutor(ContainerJobExecutor):

    SUPPORTED_PROVISIONED_TYPES = ['docker']

    def __init__(self, job, provider, workload, executor_context, job_started_callback, job_finished_callback):
        super().__init__(job, provider, workload, executor_context, job_started_callback, job_finished_callback)
        self.provider = provider.get_client()  # because the provider argument contains the execution environment

    # TODO: can be moved in the provisioning/deprovisioning steps?
    def _before_execute_job(self):
        if 'network' not in self.provider_context:
            common_labels = {f'benchsuite.session.{k}': v for k, v in self.executor_context['props']['session'].items()}
            self.provider_context['network'] = self.provider.create_network(self.executor_context['id'], labels=common_labels)

    def _start_service(self):
        cid = self._create_container()

        if not self.provider.start_container(cid, wait_until_running=True):
            return {'status': -1, 'error': 'Cannot start container'}

        if self.job_c.readinessCmd:
            readiness_status = self.provider.wait_container_readiness(cid, self.job_c.readinessCmd)
            if readiness_status.exit_code !=0:
                return {'status': -2, 'error': 'Readiness probe failed'}

        return {
            'status': 0,
            'ip': self.provider.container_ip(cid, network=self.provider_context['network'])
        }

    def _initialize_multi_job_container(self):
        # build container with info from the first job

        cid = self._create_container(name_override=f'{self.component}-multi-job',
                                     entrypoint_override='/bin/sh', command_override=['-c', 'sleep 1d'])

        if not self.provider.start_container(cid, wait_until_running=True):
            logger.error(f'Error starting container {cid}. TODO: collect and print logs')
            return False

        self.component_context['multi_job_container'] = {'cid': cid}

    def _execute_in_multi_job_container(self):
        command = self.job_c.cmd

        # do not expand env variables here because this will be executed in a shell
        if command:
            if self.job_c.args:
                command = command + ' ' + ' '.join(self.job_c.args)
        if self.job_c.shellCmd:
            #command = self.__expand_env_var(job_c.shellCmd, component_context['multi_job_container']['env'])
            command = self.job_c.shellCmd

        cmd = ['/bin/sh', '-c', command]

        self.job_started_callback(self.job)
        res = self.provider.exec_container(self.component_context['multi_job_container']['cid'], cmd, redirect_to_logs=True)
        self.job_finished_callback(self.job)

        self.job_context['status'] = res

        return res['ret_val']

    def _execute_batch(self):

        cid = self._create_container()

        if not self.provider.start_container(cid, wait_until_running=True):
            logger.error(f'Error starting container {cid}. TODO: collect and print logs')
            return False

        if self.job_c.readinessCmd:
            readiness_status = self.provider.wait_container_readiness(cid, self.job_c.readinessCmd)
            if readiness_status.exit_code != 0:
                logger.error(f'Container is not ready after timeout expired.')
                return False

        #profiling_job = self.provider.start_container_profiling(cid, {'workload': self.workload.name, 'tool': self.workload.tool, 'version': self.workload.version, 'wid': self.workload.wid, 'categories': self.workload.categories})
        self.job_started_callback(self.job)
        res = self.provider.wait_container(cid)
        #profile = self.provider.stop_container_profiling(profiling_job)
        self.job_finished_callback(self.job)

        #job_context['profiles'].append(profile)
        return res

    def start_job_profiling(self, current_job):

        tags = {'workload': self.workload.name, 'tool': self.workload.tool, 'version': self.workload.version,
                'wid': self.workload.bsid, 'provider': self.provider.name, 'execution-id': self.executor_context['id'],
                'job': self.job.name, 'triggering_job': current_job.name, 'sut': self.job.is_sut()}

        if 'cid' in self.job_context:
            cid = self.job_context['cid']
        else:
            # assuming it is running in a multi-job-container
            cid = self.component_context['multi_job_container']['cid']
        profiling_job = self.provider.start_container_profiling(cid, tags)
        if profiling_job:
            self.job_context['active_profiling_job'] = profiling_job

    def stop_job_profiling(self):
        if 'active_profiling_job' not in self.job_context:
            return

        profile = self.provider.stop_container_profiling(self.job_context['active_profiling_job'])
        self.job_context['profiles'].append(profile)

    def _create_container(self, name_override=None, entrypoint_override=None, command_override=None):

        final_env = dict(self.job_c.env)
        final_env.update(self.executor_context['env'])

        for k, v in final_env.items():
            final_env[k] = self._expand_env_var(v, final_env)

        entrypoint = self.job_c.cmd
        command = [self._expand_env_var(a, final_env) for a in self.job_c.args]
        if self.job_c.shellCmd:
            entrypoint = '/bin/sh'
            command = ['-c', self.job_c.shellCmd]

        if entrypoint_override:
            entrypoint = entrypoint_override
        if command_override:
            command = command_override

        name = name_override if name_override else self.job.name

        security_opts = self.job_c.security.dict()

        volume_mounts = []
        for m in self.job_c.mounts:
            volume_mounts.append(
                {'name': m.name or random_id(), 'type': m.type, 'path': m.hostPath, 'mountPath': m.mountPath, 'readOnly': m.readOnly})

        common_labels = {f'benchsuite.session.{k}':v for k,v in self.executor_context['props']['session'].items()}

        cid = self.provider.create_container(self.job_c.image, name=name, entrypoint=entrypoint, command=command, network=self.provider_context['network'], environment=final_env, volume_mounts=volume_mounts, security_opts=security_opts, labels=common_labels)
        self.job_context['cid'] = cid
        self.job_context['env'] = final_env

        return cid

    def get_job_profiles(self):
        return self.job_context['profiles']

    def get_job_status(self):
        if 'status' in self.job_context:
            # if the status has been already collected (e.g. jobs in multi-job-containers)
            status = dict(self.job_context['status'])
        else:
            if 'cid' in self.job_context:
                status = self.provider.get_container_status(self.job_context['cid'])
            else:
                logger.warning('Exception in getting job status from context. Probably the job has not been executed')
                return {'ret_val': -1, 'runtime': -1, 'stdout': None, 'stderr': None, 'node': None}

        return status

    def cleanup_job(self):

        if 'components' not in self.provider_context or self.component not in self.provider_context['components']:
            return

        component_context = self.provider_context['components'][self.component]
        job_context = component_context['jobs'].get(self.job.name, None)
        if not job_context or 'cid' not in job_context:
            return

        if job_context:

            if 'multi_job_container' in component_context:
                del component_context['jobs'][self.job.name]
                if not component_context['jobs']:
                    self.provider.remove_container(component_context['multi_job_container']['cid'], stop_if_running=True)
            else:
                self.provider.remove_container(job_context['cid'], stop_if_running=True)
                del component_context['jobs'][self.job.name]

            # if there are no more jobs, we delete the component context (we need it to know when delete the network)
            if not component_context['jobs']:
                del self.provider_context['components'][self.component]

        if not self.provider_context['components'] and 'network' in self.provider_context:
            # last one. We can also delete the network
            self.provider.remove_network(self.provider_context['network'])
            del self.provider_context['network']
