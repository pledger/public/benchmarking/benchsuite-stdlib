#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

from benchsuite.core.util.strings import random_id
from benchsuite.stdlib.workloads.jb.executors.common import ContainerJobExecutor

logger = logging.getLogger(__name__)


class KubernetesContainerJobExecutor(ContainerJobExecutor):

    SUPPORTED_PROVISIONED_TYPES = ['kubernetes']

    def __init__(self, job, provider, workload, executor_context, job_started_callback, job_finished_callback):
        super().__init__(job, provider, workload, executor_context, job_started_callback, job_finished_callback)
        self.provider = provider.get_client()  # because the "provider" argument in reality is the execution environment

    def _get_common_params(self, name_override=None, cmd_override=None, args_override=None, needs_profiling_override=None):
        job_c = self.job_c

        final_env = dict(job_c.env)
        final_env.update(self.executor_context['env'])

        for k, v in final_env.items():
            final_env[k] = self._expand_env_var(v, final_env)

        name = name_override if name_override else f'bsexec-{self.executor_context["id"]}-{self.job.name}'
        node_selector = self.provider_context['params'].get('k8s_node', None)

        if job_c.shellCmd:
            cmd = '/bin/sh'
            args = ['-c', job_c.shellCmd]
        else:
            cmd = job_c.cmd
            args = job_c.args
            # if the entrypoint is not a shell and the args contain env variables, make them compatible with Kubernetes
            if args and (cmd != '/bin/bash' and cmd != '/bin/sh'):
                args = [self._expand_env_var(a, final_env) for a in args]

        if cmd_override:
            cmd = cmd_override

        if args_override:
            args = args_override

        needs_profiling = self.job.profile

        if needs_profiling_override is not None:
            needs_profiling = needs_profiling_override

        volume_mounts = []
        for m in job_c.mounts:
            volume_mounts.append(
                {'name': m.name or random_id(), 'type': m.type, 'path': m.hostPath, 'mountPath': m.mountPath, 'readOnly': m.readOnly})

        security_opts = {
            'host_pid': job_c.security.enableHostPID,
            'host_network': job_c.security.enableHostNetwork
        }

        common_labels = {f'benchsuite.session.{k}': v for k, v in self.executor_context['props']['session'].items()}
        # sanitize label values (e.g. ":" is not a valid character)
        for k, v in common_labels.items():
            common_labels[k] = v.replace(":", "__semicolon__")
        print(common_labels)

        return {
            'image': job_c.image,
            'namespace': self.provider_context['params']['namespace'],
            'kwargs': {'name': name, 'node_selector': node_selector, 'cmd': cmd, 'args': args, 'env': final_env, 'readiness_cmd': job_c.readinessCmd, 'needs_profiling': needs_profiling, 'volume_mounts': volume_mounts, 'security_opts': security_opts, 'labels': common_labels}
        }

    def _initialize_multi_job_container(self):
        # build container with info from the first job
        job = self.jobs_in_component[0]
        fake_context = {}

        # we need profiling prepared if at least one job needs profiling
        needs_profiling = False
        for j in self.jobs_in_component:
            if j.profile:
                needs_profiling = True
                break

        params = self._get_common_params(name_override=f'bsexec-{self.executor_context["id"]}-{self.component}-multi-job',
                                     cmd_override='/bin/sh', args_override=['-c', 'sleep 1d'],
                                     needs_profiling_override=needs_profiling)

        res = self.provider.create_job(params['image'], params['namespace'], **params['kwargs'], restart_policy='Never')
        fake_context['job'] = res
        self.component_context['multi_job_container'] = fake_context

    def _execute_in_multi_job_container(self):

        command = self.job_c.cmd
        if command:
            if self.job_c.args:
                command = command + ' ' + ' '.join(self.job_c.args)
        if self.job_c.shellCmd:
            #command = self.__enclose_env_vars_with_parenthesis(job_c.shellCmd)
            command = self.job_c.shellCmd

        cmd = ['/bin/sh', '-c', command]
        self.job_started_callback(self.job)
        res = self.provider.job_exec_cmd(self.component_context['multi_job_container']['job']['bsname'], self.provider_context['params']['namespace'], cmd, redirect_to_logs=True)
        self.job_finished_callback(self.job)

        self.job_context['status'] = res

        return res['ret_val']

    def _start_service(self):

        params = self._get_common_params()

        res = self.provider.create_deployment(params['image'], params['namespace'], **params['kwargs'])

        self.job_context['deployment'] = res

        if res['status'] != 0:
            return res

        if self.job.port:
            self.provider.create_service(self.job.name, self.provider_context['params']['namespace'], 'None',
                                         {'bsname': res['bsname']},
                                         self.job.port, self.job.port, 'TCP', labels=params['kwargs']['labels'])
            self.job_context['service'] = self.job.name

        return res

    def _execute_batch(self):

        params = self._get_common_params()

        res = self.provider.create_job(params['image'], params['namespace'], **params['kwargs'], restart_policy='Never')
        self.job_context['job'] = res

        job_name = res['bsname']
        #profiling_job = self.provider.start_job_profiling(job_name, provider_context['params']['namespace'], {'workload': self.workload.name, 'tool': self.workload.tool, 'version': self.workload.version, 'wid': self.workload.wid, 'categories': self.workload.categories})
        self.job_started_callback(self.job)
        #profile = self.provider.stop_container_profiling(profiling_job)
        res = self.provider.wait_job(job_name, self.provider_context['params']['namespace'])
        self.job_finished_callback(self.job)
        #profile = self.provider.stop_job_profiling(profiling_job)
        # TODO: publish profile
        return res['status']

    def start_job_profiling(self, current_job):

        tags = {'workload': self.workload.name, 'tool': self.workload.tool, 'version': self.workload.version,
                'wid': self.workload.bsid, 'provider': self.provider.name, 'execution-id': self.executor_context['id'],
                'job': self.job.name, 'triggering_job': current_job.name, 'sut': self.job.is_sut()}

        if 'deployment' in self.job_context and 'deployment' in self.job_context:
            job_name = self.job_context['deployment']['bsname']
            profiling_job = self.provider.start_deployment_profiling(job_name, self.provider_context['params']['namespace'], tags)
        elif 'job' in self.job_context and 'job' in self.job_context:
            job_name = self.job_context['job']['bsname']
            profiling_job = self.provider.start_job_profiling(job_name, self.provider_context['params']['namespace'], tags)
        elif self.is_multi_job_component and 'job' in self.component_context['multi_job_container']:
            job_name = self.component_context['multi_job_container']['job']['bsname']
            profiling_job = self.provider.start_job_profiling(job_name, self.provider_context['params']['namespace'], tags)
        else:
            logger.error('Cannot start profiling of job %s because not corresponding container to profile was found', self.job.name)
            profiling_job = None

        if profiling_job:
            self.job_context['active_profiling_job'] = profiling_job

    def stop_job_profiling(self):
        if 'active_profiling_job' not in self.job_context:
            return

        profile = self.provider.stop_job_profiling(self.job_context['active_profiling_job'])
        self.job_context['profiles'].append(profile)

    def get_job_status(self):

        if 'status' in self.job_context:
            # if the status has been already collected (e.g. jobs in multi-job-containers
            status = dict(self.job_context['status'])
        else:
            if self.job.type == 'service' and 'deployment' in self.job_context:
                status = self.provider.get_deployment_status(self.job_context['deployment']['bsname'], self.provider_context['params']['namespace'])
            else:
                if 'job' in self.job_context:
                    status = self.provider.get_job_status(self.job_context['job']['bsname'], self.provider_context['params']['namespace'])
                else:
                    status = {'ret_val': -1, 'runtime': -1, 'stdout': None, 'stderr': 'Job not executed', 'node': None}

        return status

    def get_job_profiles(self):
        return self.job_context['profiles']

    def cleanup_job(self):

        if 'components' not in self.provider_context or self.component not in self.provider_context['components']:
            return

        component_context = self.provider_context['components'][self.component]
        job_context = component_context['jobs'].get(self.job.name, None)
        if not job_context:
            return

        if job_context:

            if 'multi_job_container' in component_context:
                del component_context['jobs'][self.job.name]
                if not component_context['jobs']:
                    # multi_job_container without jobs
                    self.provider.delete_job(component_context['multi_job_container']['job']['bsname'], self.provider_context['params']['namespace'])

            else:
                if self.job.type == 'service' and 'deployment' in job_context:
                    self.provider.delete_deployment(job_context['deployment']['bsname'], self.provider_context['params']['namespace'])
                    if 'service' in job_context:
                        self.provider.delete_service(job_context['service'], self.provider_context['params']['namespace'])
                if self.job.type == 'batch' and 'job' in job_context:
                    self.provider.delete_job(job_context['job']['bsname'], self.provider_context['params']['namespace'])
                del component_context['jobs'][self.job.name]

            # if there are no more jobs, we delete the component context (we need it to know when delete the network)
            if not component_context['jobs']:
                del self.provider_context['components'][self.component]