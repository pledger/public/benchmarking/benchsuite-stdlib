#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging
import time
import datetime
import os

from benchsuite.stdlib.utils.timeutils import convert_to_h_m_s
from benchsuite.stdlib.workloads.jb.executors.common import JobExecutor

logger = logging.getLogger(__name__)


class ShellJobExecutor(JobExecutor):

    SUPPORTED_FORMS = ['shell']
    SUPPORTED_PROVISIONED_TYPES = ['ssh']

    def execute_job(self):
        logger.debug(f'Executing command through ssh: {self.job_c.command}')
        if self.job_c.sudo_as_user:
            logger.debug('Script will be executed as use {0} using sudo'.format(self.job_c.sudo_as_user))
        exit_code = self.__execute_cmd(self.provider.vm, self.job_c.command, self.job.name, sudo_user=self.job_c.sudo_as_user)
        out = self.__get_cmd_output(self.provider.vm, 'cat ' + self._get_filename('run', 'cmd_stdout'))
        err = self.__get_cmd_output(self.provider.vm, 'cat ' + self._get_filename('run', 'cmd_stderr'))
        runtime = self.get_runtime(None, self.provider.vm)
        self.job_context['status'] = {'stdout': out, 'stderr': err, 'ret_val': exit_code, 'node': 'NODE-0', 'runtime': runtime}
        return exit_code

    def get_job_status(self):
        return self.job_context['status']

    def get_job_profiles(self):
        return []

    def cleanup_job(self):
        pass

    def __execute_cmd(self, vm, cmd, phase, _async=False, poll_for_termination=True, sudo_user=None):
        '''

        :param vm:
        :param cmd:
        :param phase:
        :param async: return immediately after the command has been launched
        :param poll_for_termination: if False, wait until the command is finished
        keeping the ssh connection up (that might lead to problems). If True,
        checks periodically if the lock file still exists
        :return: the exit code or 0 if async == True
        '''

        logger.info('Executing "{0}" commands on vm "{1}"'.format(phase, vm.id))

        remote_script = self.__generate_remote_script(vm, cmd, phase, sudo_user=sudo_user)

        # if poll_for_termination, we just launch the command and then check
        # when it is finished by ourselves
        exit_status, stdout, stderr = self.run_ssh_cmd(vm, remote_script, _async=poll_for_termination or _async)

        if _async:
            logger.info('Execution launched. Since async=True return immediately')
            return 0

        if poll_for_termination:
            waited_time = self._wait_for_cmd(vm, phase)

            if logger.isEnabledFor(logging.DEBUG):
                logger.debug('Waited vs. Actual runtime: {0} vs. {1} seconds'.format(
                    int(waited_time.total_seconds()), self.get_runtime(phase, vm=vm)))

        exit_status = int(self.__get_cmd_output(vm, 'cat ' + self._get_filename(phase, 'cmd_retcode')))

        logger.info('Execution exited with status code {0}'.format(exit_status))

        if not exit_status == 0:
            cmd_out = self.__get_cmd_output(vm, 'cat ' + self._get_filename(phase, 'cmd_stdout'))
            cmd_err = self.__get_cmd_output(vm, 'cat ' + self._get_filename(phase, 'cmd_stderr'))

            e = Exception(
                f'Command "{cmd}" exit with status {exit_status}. The stderr is: "{cmd_err}"')
            e.cmd = cmd
            e.exit_status = exit_status
            e.stdout = cmd_out
            e.stderr = cmd_err
            raise e

        return exit_status

    def run_ssh_cmd(self, vm, cmd, _async=False):
        # do not user env_dict because it is valid only for commands executed
        # directly, but we execute a script inside a new bash
        return self.provider.execute_cmd(cmd, env_dict=None)

    def get_runtime(self, phase, vm = None):
        if not vm:
            if 'default' in self.env.vms:
                vm = self.env.vms['default']
            else:
                vm = self.env.vms[self.test._props['vm_list'][-1]]
        return int(self.__get_cmd_output(vm, 'cat ' + self._get_filename(phase, 'cmd_time')))

    def __get_cmd_output(self, vm, cmd):
        exit_status, stdout, stderr = self.run_ssh_cmd(vm, cmd)
        return stdout

    @staticmethod
    def __get_sleeptime(step):
        if step < 6:
            return 10
        if step < 20:
            return 30
        if step < 70:
            return 60

        return 300

    def _get_filename(self, phase, type):
        extensions = {
            'cmd_stdout': 'out',
            'cmd_stderr': 'err',
            'cmd_script': 'sh',
            'cmd_wrapper_script': 'wrapper.sh',
            'cmd_lock': 'lock',
            'cmd_time': 'time',
            'cmd_retcode': 'ret'
        }
        return '/tmp/{0}-{1}.{2}'.format(self.executor_context["id"], self.job.name, extensions[type])

    def _wait_for_cmd(self, vm, phase):
        t_start = datetime.datetime.now()

        # wait for 5 seconds to be sure that the lock file has been created
        time.sleep(5)

        lock_exists, _, _ = self.run_ssh_cmd(vm, 'test ! -f ' + self._get_filename(phase, 'cmd_lock'))
        c = 0
        while lock_exists:
            c += 1
            sleep_time = ShellJobExecutor.__get_sleeptime(c)
            running_time = datetime.datetime.now() - t_start
            if logger.isEnabledFor(logging.INFO):
                logger.info('Running since {0}. Lock file still exists. '
                            'Waiting for {1} seconds'.format(
                    convert_to_h_m_s(running_time.total_seconds()), sleep_time))
            time.sleep(sleep_time)
            lock_exists, _, _ = self.run_ssh_cmd(vm, 'test ! -f ' + self._get_filename(phase, 'cmd_lock'))

            default_timeout = 1200  # 20 minutes
            if running_time.total_seconds() > default_timeout:
                raise Exception("Execution is taking too long: {0}. Aborting.".format(
                    convert_to_h_m_s(running_time.total_seconds())))

        return datetime.datetime.now() - t_start

    def __generate_remote_script(self, vm, cmd, phase, sudo_user: None):

        script = self._get_filename(phase, 'cmd_script')
        script_wrapper = self._get_filename(phase, 'cmd_wrapper_script')
        lock = self._get_filename(phase, 'cmd_lock')
        ret = self._get_filename(phase, 'cmd_retcode')
        out = self._get_filename(phase, 'cmd_stdout')
        err = self._get_filename(phase, 'cmd_stderr')
        runtime = self._get_filename(phase, 'cmd_time')

        working_dir = vm.working_dir + os.path.sep + self.job.name

        # removes empty lines
        cmd = os.linesep.join([s for s in cmd.splitlines() if s])

        env_export = '\n'.join([f'export {k}="{v}"' for k, v in self.get_environment_dict().items()])

        if sudo_user:
            sudo_str = f'sudo -u {sudo_user} '
        else:
            sudo_str = ''

        decorated_cmd = '''cat << 'EOF' > {0}
touch {1}
mkdir -p {2}
cd {2}
cat << 'END2' > {3}
set -e
{9}
{4}
END2
SECONDS=0
{10}bash -e {3} 1> {5} 2> {6}
echo $? > {7}
echo $SECONDS > {8}
rm {1}
exit `cat {7}`
EOF
bash {0}'''.format(script_wrapper, lock, working_dir, script, cmd, out,
                   err, ret, runtime, env_export, sudo_str)

        decorated_cmd += '\n'

        return decorated_cmd

