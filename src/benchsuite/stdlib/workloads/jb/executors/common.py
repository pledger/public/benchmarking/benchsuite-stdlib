#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging
import re

logger = logging.getLogger(__name__)


class JobExecutor(object):

    def __init__(self, job, provider, workload, executor_context, job_started_callback, job_finished_callback):
        self.job = job
        self.provider = provider
        self.workload = workload
        self.executor_context = executor_context
        self.job_started_callback = job_started_callback
        self.job_finished_callback = job_finished_callback

        # because we only have the default provider at the moment
        self.provider_context = self.executor_context['provider.default']

        form = self.SUPPORTED_FORMS[0]

        self.job_c = job.forms[form]
        self.component = self.job_c.topology.component
        self.jobs_in_component = self.workload.jobs.by_component(self.provider_context['name'], self.component, form)
        self.is_multi_job_component = len(self.jobs_in_component) > 1

        if 'components' not in self.provider_context:
            self.provider_context['components'] = {}

        if self.component not in self.provider_context['components']:
            self.provider_context['components'][self.component] = {'jobs': {}}

        self.component_context = self.provider_context['components'][self.component]
        if job.name not in self.component_context['jobs']:
            self.component_context['jobs'][job.name] = {'profiles': []}
        self.job_context = self.component_context['jobs'][job.name]

    def get_environment_dict(self):
        final_env = dict(self.job_c.env)
        final_env.update(self.executor_context['env'])

        for k, v in final_env.items():
            final_env[k] = self._expand_env_var(v, final_env)

        return final_env

    def _expand_env_var(self, string, env):
        if not string:
            return None
        regex = r"(\$([a-zA-Z_][a-zA-Z0-9_]*?))(\s|$|[^a-zA-Z0-9_])"

        matches = re.finditer(regex, string)
        for matchNum, match in enumerate(matches, start=1):
            var_name = match.group(2)
            if var_name in env:
                string = string.replace(match.group(1), str(env[var_name]))
                logger.debug(f'Variable "{var_name}" expanded in "{env[var_name]}"')

        return string

    def execute_job(self):

        self._before_execute_job()

        if self.is_multi_job_component:
            if 'multi_job_container' not in self.component_context:
                self._initialize_multi_job_container()
            return self._execute_in_multi_job_container()
        else:
            if self.job.type == 'service':
                return self._start_service()
            else:
                return self._execute_batch()

    def _before_execute_job(self):
        pass

    def get_job_status(self):
        raise NotImplementedError()

    def get_job_profiles(self):
        raise NotImplementedError()

    def cleanup_job(self):
        raise NotImplementedError()


class ContainerJobExecutor(JobExecutor):

    SUPPORTED_FORMS = ['container']

    # same of Docker Executor!!!
    def _expand_env_var(self, string, env):
        if not string:
            return None
        regex = r"(\$([a-zA-Z_][a-zA-Z0-9_]*?))(\s|$|[^a-zA-Z0-9_])"

        matches = re.finditer(regex, string)
        for matchNum, match in enumerate(matches, start=1):
            logger.warn(f'Found env variable "{match.group(1)}" in container args. Kubernetes supports expansion only in args and only if enclosed in $(). For uniformity with Docker we expand all variables')
            var_name = match.group(2)
            if var_name in env:
                string = string.replace(match.group(1), str(env[var_name]))
                logger.debug(f'Variable "{var_name}" expanded in "{env[var_name]}"')

        return string

