#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging
from importlib import metadata

from attrdict import AttrDict

from benchsuite.core.model.benchmark import WorkloadController

logger = logging.getLogger(__name__)


class JBWorkloadController(WorkloadController):

    SUPPORTED_WORKLOADS = ['jb-workload']

    def __init__(self, execution):
        self.execution = execution
        self.workload = self.execution.workload
        self.context = self.execution.context

        self.context['provider.default'] = {
            'name': 'default',
            'params': self.execution.params['provider.default']
        }

        self.provider_default_context = self.context['provider.default']
        self.context['id'] = self.execution.id
        self.context['env'] = {}
        self.executor_env = self.context['env']
        self.profiles = {}
        self.final_status = None

    def on_job_started(self, current_job):
        if not current_job.phase == 'run':
            return

        jobs_to_profile = self.workload.jobs.filter(profile=True)
        for j in jobs_to_profile:
            logger.info('Starting profiling of job %s', j.name)
            jexec = self._get_job_executor(j, self.provider_default)
            res = jexec.start_job_profiling(current_job)

    def on_job_finished(self, current_job):
        if not current_job.phase == 'run':
            return
        # we profile the current job (if it is sut) and any service that is sut
        jobs_to_profile = self.workload.jobs.filter(profile=True)
        for j in jobs_to_profile:
            logger.info('Starting profiling of job %s', j.name)
            jexec = self._get_job_executor(j, self.provider_default)
            res = jexec.stop_job_profiling()

    def setup(self, dry_run=False):
        for s in self.workload.jobs.filter(phase='services', form=self._get_executable_form()):
            logger.info('Starting service %s', s.name)
            jexec = self._get_job_executor(s, self.provider_default)
            if dry_run:
                return
            res = jexec.execute_job()
            if res['status'] != 0:
                raise Exception('Execution of job {0} failed: {1}'.format(s.name, res))
            self.executor_env[f'BS_{s.name.upper()}_IP'] = str(res['ip'])
            if s.port:
                self.executor_env[f'BS_{s.name.upper()}_PORT'] = str(s.port)
            logger.info(f'Service {s.name} started with IP {res["ip"]}')

    def _get_executable_form(self):
        type = self.context['execenvs']['default'].TYPE
        supported_forms = set()
        for ep in metadata.entry_points()['benchsuite.jb.executors']:
            clazz = ep.load()
            if type in clazz.SUPPORTED_PROVISIONED_TYPES:
                for f in clazz.SUPPORTED_FORMS:
                    supported_forms.add(f)

        logger.debug('Supported forms are %s', supported_forms)

        # return the first one
        return next(iter(supported_forms))

    def prepare(self, dry_run=False):
        for j in self.workload.jobs.filter(phase='prepare', form=self._get_executable_form()):
            logger.info('Executing prepare job %s', j.name)
            jexec = self._get_job_executor(j, self.provider_default)
            if dry_run:
                return
            res = jexec.execute_job()
            if res != 0:
                raise Exception('Execution of job {0} failed with exit code {1}'.format(j.name, res))

    def run(self, dry_run=False):
        for j in self.workload.jobs.filter(phase='run', form=self._get_executable_form()):
            logger.info('Executing run job %s', j.name)
            jexec = self._get_job_executor(j, self.provider_default)
            if dry_run:
                return
            res = jexec.execute_job()
            if res != 0:
                raise Exception('Execution of job {0} failed with exit code {1}'.format(j.name, res))

    def collect(self, dry_run=False):
        self.context['final_status'] = []
        self.context['profiles'] = []
        for j in self.workload.jobs.filter(form=self._get_executable_form()):
            logger.info('Getting status for job %s', j.name)
            jexec = self._get_job_executor(j, self.provider_default)
            if dry_run:
                continue
            status = jexec.get_job_status()

            # add for retro compatibility
            status['step'] = 'setup' if j.phase == 'services' else j.phase
            status['component'] = j.name

            self.context['final_status'].append(status)

            self.context['profiles'].extend(jexec.get_job_profiles())

    def get_execution_status_info(self):
        pass

    def get_describe_object(self):
        statuses = {c['component']: c for c in self.context['final_status']}
        if self.workload.jobs.sut_job and self.workload.jobs.sut_job.name in statuses:
            sut_nodes = [statuses[self.workload.jobs.sut_job.name]['node']]
            self.provider_default.model.tags['node'] = sut_nodes[0]
        else:
            sut_nodes = []
        return AttrDict({
            'components': statuses,
            'sut_nodes': sut_nodes,
            'profiles': self.context['profiles']
        })

    def cleanup(self, dry_run=False):
        # TODO: remove only jobs created
        for j in self.workload.jobs.filter(form=self._get_executable_form()):
            logger.info('Cleaning-up job %s', j.name)
            jexec = self._get_job_executor(j, self.provider_default)
            jexec.cleanup_job()

    def get_supported_provisioned_types(self):
        available_forms = set()

        for j in self.workload.jobs.all():
            for f in j.forms.supported():
                available_forms.add(f)

        res = []
        for ep in metadata.entry_points()['benchsuite.jb.executors']:
            clazz = ep.load()
            if [v for v in available_forms if v in clazz.SUPPORTED_FORMS]:
                res.extend(clazz.SUPPORTED_PROVISIONED_TYPES)
        return res

    def _get_job_executor(self, job, provider, execution_constrained_form=None):

        if not provider:
            raise Exception('Provider is not set! Cannot run workload jobs')

        # 1. get job forms
        job_supported_forms = job.forms.supported()
        if execution_constrained_form:
            if execution_constrained_form in job_supported_forms:
                job_supported_forms = [execution_constrained_form]
            else:
                job_supported_forms = []

        # 2. get provider driver
        provider_driver = provider.TYPE

        job_executor = None
        for f in job_supported_forms:
            for ep in metadata.entry_points()['benchsuite.jb.executors']:
                clazz = ep.load()
                if provider_driver in clazz.SUPPORTED_PROVISIONED_TYPES and f in clazz.SUPPORTED_FORMS:
                    job_executor = clazz
                    break
            if job_executor:
                break

        if job_executor:
            logger.info('Job Executor selected for driver "%s" and forms %s: %s',
                        provider_driver, job_supported_forms, job_executor.__module__ + '.' + job_executor.__qualname__)
        else:
            raise Exception('Could not find a Job Executor for driver "%s" and forms %s', provider_driver,
                         job_supported_forms)
        return job_executor(job, provider, self.workload,  self.context, self.on_job_started, self.on_job_finished)
