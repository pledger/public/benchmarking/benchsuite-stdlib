#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging
from importlib import metadata
from typing import List, Dict, Any, Union, TypedDict, ClassVar, Optional

from pydantic import BaseModel, Field, Extra, PrivateAttr, validator

from benchsuite.core.model.benchmark import Workload

logger = logging.getLogger(__name__)


class JBGenericRegexParser(BaseModel, extra=Extra.forbid):
    name: str
    params: dict = {}


class JBTopologyModel(BaseModel, extra=Extra.forbid):

    provider: str = None
    component: str = None

    def get_overridden_copy(self, other):
        if not other:
            return self

        return JBTopologyModel(
           provider=other.provider if other.provider else self.provider,
           component=other.component if other.component else self.component
        )


class JobFormBaseModel(BaseModel):

    env: Dict[str, str] = {}
    topology: JBTopologyModel = JBTopologyModel()

    _raw_env: Dict[str, str] = PrivateAttr()
    _raw_topology: JBTopologyModel = PrivateAttr()

    def _init_merged_fields(self, upper_env, upper_topology, default_component_name):

        # env
        self._raw_env = dict(self.env)
        self.env = dict(upper_env)
        self.env.update(self._raw_env)

        # topology
        self._raw_topology = self.topology
        self.topology = upper_topology.get_overridden_copy(self.topology) if upper_topology else self.topology
        if not self.topology.component:
            self.topology.component = default_component_name
        if not self.topology.provider:
            self.topology.provider = 'default'


class ContainerSecurityModel(BaseModel, extra=Extra.forbid):
    enableHostPID: bool = False
    enableHostNetwork: bool = False
    enableHostUserNS: bool = False  # not implemented for K8s (but possible)
    capabilities: List[str] = []  # not implemented for K8s (but possible)


class ContainerVolumeMountModel(BaseModel, extra=Extra.forbid):
    hostPath: str
    mountPath: str = None
    readOnly: bool = True
    type: str = 'hostPath'
    name: str = None

    # if omitted, mountPath will be the same of hostPath
    @validator('mountPath', pre=True, always=True)
    def default_ts_modified(cls, v, *, values, **kwargs):
        return v or values['hostPath']


class JobFormContainerModel(JobFormBaseModel, extra=Extra.forbid):

    image: str = None
    cmd: str = None
    args: List[str] = []
    shellCmd: str = None
    readinessCmd: List[str] = None
    readinessBashCmd: str = None
    security: ContainerSecurityModel = ContainerSecurityModel()
    mounts: List[ContainerVolumeMountModel] = []

    def __init__(self, **data: Any):
        super().__init__(**data)
        if self.readinessBashCmd and not self.readinessCmd:
            self.readinessCmd = ['/bin/bash', '-ec', self.readinessBashCmd]


class JobFormShellModel(JobFormBaseModel, extra=Extra.forbid):

    sudo_as_user: str = Field(None, alias='sudo-as-user')
    command: str = None


class JobFormsModel(BaseModel, extra=Extra.forbid):
    __root__: Dict[str, Union[JobFormContainerModel, JobFormShellModel]]

    def __iter__(self):
        return iter(self.__root__)

    def __getitem__(self, item):
        return self.__root__[item]

    def items(self):
        return self.__root__.items()

    def supported(self):
        return list(self.__root__.keys())


class JBJobModel(BaseModel, extra=Extra.forbid):

    name: str = None
    type: str
    phase: str
    forms: JobFormsModel
    port: int = None
    profile: bool = None

    env: Dict[str, str] = {}
    topology: JBTopologyModel = None

    roles: List[str] = []
    parser: Union[str, JBGenericRegexParser] = None

    @validator('parser')
    def parser_init(cls, v):
        if isinstance(v, str):
            parser_name = v
            params = {}
        else:
            parser_name = v.name
            params = v.params

        clazz = None
        for ep in metadata.entry_points()['benchsuite.parser']:
            if ep.name == parser_name:
                clazz = ep.load()
                break
        if clazz:
            return clazz(config=params)
        else:
            logger.debug('Could not instantiate a parser for value: %s', v)
            return None

    def is_sut(self):
        return 'sut' in self.roles


class JBJobsModel(BaseModel, extra=Extra.forbid):

    __root__: List[JBJobModel]

    def __iter__(self):
        return iter(self.__root__)

    def __getitem__(self, item):
        return self.__root__[item]

    def all(self):
        return self.filter()

    def first(self, **kwargs):
        res = self.filter(**kwargs)
        if res:
            return res[0]
        else:
            return None

    def filter(self, name=None, phase=None, has_parser=None, type=None, profile=None, role=None, form=None):
        filtered = []
        for j in self.__root__:
            if name and not j.name == name:
                continue
            if phase and not j.phase == phase:
                continue
            if has_parser and not j.parser:
                continue
            if type and not j.type == type:
                continue
            if profile and not j.profile == profile:
                continue
            if form and not form in j.forms:
                continue
            # TODO: make it possible for role to be a list
            if role is not None and role not in j.roles:
                continue
            filtered.append(j)
        return filtered

    def by_component(self, provider, component, form):
        res = []
        for j in self.__root__:
            if form in j.forms:
                if j.forms[form].topology.component == component and  j.forms[form].topology.provider == provider:
                    res.append(j)
        return res

    @property
    def services(self):
        return self.filter(phase='services')

    @property
    def prepare(self):
        return self.filter(phase='prepare')

    @property
    def run(self):
        return self.filter(phase='run')

    @property
    def sut_job(self):
        return self.first(role='sut')


class GrantsModel(BaseModel):
    executor: str = None
    owner: str = None


class JBWorkload(Workload, extra=Extra.forbid):

    id: Optional[str] = None
    tool: str
    name: str
    version: str
    revision: Optional[str] = None

    driver: str

    license: Optional[str] = None
    license_url: Optional[str] = Field(None, alias='license-url')
    maintainer_name: Optional[str] = Field(None, alias='maintainer-name')
    maintainer_email: Optional[str] = Field(None, alias='maintainer-email')
    references: Optional[Dict[str, str]] = {}

    categories: List[str] = []

    jobs: JBJobsModel

    parser: Union[str, JBGenericRegexParser] = None

    env: Dict[str, str] = {}
    topology: JBTopologyModel = None

    performance_index: str = Field(None, alias='performance-index')

    grants: GrantsModel = None

    def get_describe_object(self):
        # enrich the default describe object
        obj = super().get_describe_object()
        return obj

    # added for retro-compatibility
    def run_components_name(self):
        return [j.name for j in self.jobs.filter(phase='run')]

    def system_under_test_components(self):
        return [j.name for j in self.sut_jobs()]

    def __init__(self, **data: Any):
        super().__init__(**data)
        self._initialize()

    def _initialize(self):
        """
        complete the initialization of the model. Do all things that we couldn't achieve to do with Pydantic
        """

        #  1. build merged fields for jobs
        #     - env
        #     - topology

        default_name_counters = {'services': 0, 'prepare': 0, 'run': 0}
        for j in self.jobs:

            if not j.name:
                c = default_name_counters[j.phase]
                default_name_counters[j.phase] = c + 1
                j.name = f'{j.phase}-{c}'

            merged_env = dict(self.env)
            merged_env.update(j.env)
            merged_topology = self.topology.get_overridden_copy(j.topology) if self.topology else j.topology
            for _, f in j.forms.items():
                f._init_merged_fields(merged_env, merged_topology, f'{j.name}-comp')

        #  2. set sut job
        sut_jobs = self.jobs.filter(role='sut')
        if len(sut_jobs) > 1:
            raise Exception('Only one sut job is allowed')

        if not sut_jobs:
            services = self.jobs.services
            run = self.jobs.run
            if len(services) == 1:
                services[0].roles.append('sut')
            if len(services) == 0 and len(run) == 1:
                run[0].roles.append('sut')

        #  3. set profile field
        if self.jobs.sut_job:
            self.jobs.sut_job.profile = True

        for j in self.jobs:
            # if profile flag is not explicitly set
            if j.profile is None:
                if j.phase == 'services':
                    j.profile = True
                if j.phase == 'run' and len(self.jobs.filter(phase='services')) == 0:
                    j.profile = True

            if j.profile is None:
                j.profile = False

            if not j.parser and self.parser:
                j.parser = j.parser_init(self.parser)

    @staticmethod
    def load_from_dict(obj):
        return JBWorkload.parse_obj(obj)
