#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging
from threading import Thread

import ciso8601

from benchsuite.core.model.profilers import Profiler

logger = logging.getLogger(__name__)


class ProfilingThread(Thread):

    def __init__(self, docker_client, container_id):
        Thread.__init__(self, args=(), kwargs=None)
        self.client = docker_client
        self.container_id = container_id
        self.data = []
        self._stop = False
        self.sampling_interval = 0.05

    def run(self):
        logger.debug('Start profiling')
        for s in self.client.containers.get(self.container_id).stats(stream=True, decode=True):
            self.data.append(s)
            if self._stop:
                break
        #while not self._stop:
        #    res = self.client.containers.get(self.container_id).stats(stream=False)
        #    self.data.append(res)
        #    time.sleep(self.sampling_interval)

    def stop(self):
        logger.debug('Profiler stopped')
        self._stop = True


class DockerStatsProfiler(Profiler):

    def __init__(self, provider):
        self.client = provider._client
        self._active_jobs = {}

    def prepare_for_profiling(self, **kwargs):
        pass

    def done_for_profiling(self, **kwargs):
        pass

    def start_profiling(self, profile_tags, **kwargs):
        container_id = kwargs['container_id']

        tags = dict(profile_tags)
        tags['profiler'] = 'docker-stats'

        thr = ProfilingThread(self.client, container_id)
        self._active_jobs[container_id] = {'thread': thr, 'tags': tags}
        thr.start()
        logger.debug('Started Docker Stats profiling')
        return container_id

    def end_profiling(self, profile_job_id):
        job = self._active_jobs[profile_job_id]
        thr = job['thread']
        thr.stop()
        logger.debug('Stopped Docker Stats profiling')
        self._active_jobs.pop(profile_job_id, None)
        return self.stats_to_profile(thr.data, job['tags'])

    @staticmethod
    def stats_to_profile(stats, tags):
        profile = {}
        t0 = DockerStatsProfiler._str_to_timestamp(stats[0]['read'])
        profile['t0'] = t0
        profile['tags'] = tags
        profile['items'] = []
        profile['units'] = {
            'cpu': 's',
            'mem': 'MB',
            'net_tx': 'MB',
            'net_rx': 'MB',
            'time': 'ms'
        }
        for i in stats:
            try:
                cpu = i['cpu_stats']['cpu_usage']['total_usage']/1000000000
                mem = i['memory_stats']['usage'] / 1000000
                if 'networks' in i:
                    net_tx = sum([v['tx_bytes'] for k, v in i['networks'].items()]) / 1000000
                    net_rx = sum([v['rx_bytes'] for k, v in i['networks'].items()]) / 1000000
                else:
                    net_rx = net_tx = None
                t1 = DockerStatsProfiler._str_to_timestamp(i['read'])
                timestamp = t1 - t0
                if timestamp.total_seconds() < 0:
                    logger.warning('Got negative timestamp in sample %s. Dropping it', i)
                    continue
                profile['items'].append({
                    'cpu': cpu,
                    'net_tx': net_tx,
                    'net_rx': net_rx,
                    'mem': mem,
                    'time': int(timestamp.total_seconds() * 1000)
                })
            except Exception as ex:
                logger.warning('Error parsing DockerStats item, discarding it: %s', str(ex))
        return profile

    @staticmethod
    def _str_to_timestamp(time_str):
        return ciso8601.parse_datetime(time_str)
