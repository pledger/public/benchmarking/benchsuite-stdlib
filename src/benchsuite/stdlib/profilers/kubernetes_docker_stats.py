#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import json
import logging
import time
import traceback
from threading import Thread

from benchsuite.core.model.profilers import Profiler
from benchsuite.core.util.strings import random_id
from benchsuite.stdlib.profilers.docker_stats import DockerStatsProfiler

logger = logging.getLogger(__name__)


class KubernetesDockerStats(Profiler):

    KEEP_PROBES = True

    class ProfilingThread(Thread):

        def __init__(self, provider, probe, container_id):
            Thread.__init__(self, args=(), kwargs=None)
            self.provider = provider
            self.probe = probe
            self.container_id = container_id
            self.data = []
            self._stop = False
            self.sampling_interval = 2

        def run(self):
            logger.debug('Start profiling')
            while not self._stop:
                try:
                    cmd = ['curl', '-s', '-G', '-X', 'GET', '--unix-socket', '/var/run/docker.sock',
                           f'http://docker/containers/{self.container_id}/stats?stream=false']
                    res = self.provider.pod_exec_cmd(self.probe['pod_name'], self.probe['namespace'], cmd)
                    stats = json.loads(res['stdout'])
                    self.data.append(stats)
                except Exception as ex:
                    logger.error('Error occurred while getting docker stats from the probe: %s', str(ex))
                    logger.error(f'Exception:\n{ex}\nTraceback:\n{traceback.format_exc()}')

                time.sleep(self.sampling_interval)

        def stop(self):
            logger.debug('Profiler stopped')
            self._stop = True

    def __init__(self, provider):
        self.provider = provider
        self.probes = {}
        self._active_jobs = {}

    def initialize(self):
        pass

    def prepare_for_profiling(self, **kwargs):
        k8s_node = kwargs['pod'].spec.node_name
        pod_name = kwargs['pod'].metadata.name

        # copy session labels from pod if exist. Only session labels because probes are at session scope
        labels = {k: v for k,v in kwargs['pod'].metadata.labels.items() if k.startswith('benchsuite.session')}

        if k8s_node not in self.probes:
            logger.debug('Creating profiling probe for node %s', k8s_node)
            self.__create_profiling_probe(k8s_node, namespace=kwargs['pod'].metadata.namespace, labels=labels)
        else:
            logger.debug('Profiling probe for node %s already exists', k8s_node)

        self.probes[k8s_node]['users'].append(pod_name)
        logger.debug('probe on node %s used by %s', k8s_node, self.probes[k8s_node]['users'])

    def __create_profiling_probe(self, k8s_node, namespace='default', labels={}):
        readiness = ['curl', '-s', '-G', '-X', 'GET', '--unix-socket', '/var/run/docker.sock', 'http://docker/version']
        volume_mounts = [{'name': 'docker-socket', 'type': 'hostPath', 'path': '/var/run/docker.sock', 'mountPath': '/var/run/docker.sock'}]

        resp = self.provider.create_job('curlimages/curl', namespace, name=f'bs-profiling-probe-{random_id()}',
                                 cmd='/bin/sh',
                                 args=['-c', 'sleep 1d'],
                                 readiness_cmd=readiness,
                                 node_selector=k8s_node, volume_mounts=volume_mounts, user_id=0, labels=labels)
        self.probes[k8s_node] = {
            'job_name': resp['bsname'],
            'namespace': namespace,
            'users': [],
            'pod_name': resp['pod_name']}

    def start_profiling(self, profile_tags, **kwargs):
        pod_name = kwargs['pod_name']
        pod_node = kwargs['pod_node']
        pod_namespace= kwargs['pod_namespace']
        container_name = 'main'
        probe = self.probes[pod_node]

        # 1. find container id
        cmd = ['curl', '-s', '-G', '-X', 'GET', '--unix-socket', '/var/run/docker.sock',
               '--data-urlencode',
               f'filters={{"label":["io.kubernetes.pod.name={pod_name}","io.kubernetes.pod.namespace={pod_namespace}","io.kubernetes.container.name={container_name}"]}}',
               'http://docker/containers/json']

        # curl -s -G -X GET --unix-socket /var/run/docker.sock --data-urlencode 'filters={"label":["io.kubernetes.pod.name=bsexec-fetojo-figipo-seponi-database-6445b5bd44-5pqjv","io.kubernetes.pod.namespace=development","io.kubernetes.container.name=main"]}' http://docker/containers/json | jq -r '.[] .Id'

        # curl -s -G -X GET --unix-socket /var/run/docker.sock http://docker/containers/600af1b90cde1addae4c88e9246fe686bb2ab683e6b0b69608f920789e0a8fa9/stats?stream=false

        # cmd = ['curl', '--unix-socket', '/var/run/docker.sock', 'http://docker/version']

        res = self.provider.pod_exec_cmd(probe['pod_name'], probe['namespace'], cmd)

        json_res = json.loads(res['stdout'])
        if len(json_res) == 0:
            logger.error('Container not found!!! Impossible to start profiling. Maybe the container already finished?')
            return None

        container_id = json_res[0]['Id']

        tags = dict(profile_tags)
        tags['profiler'] = 'k8s-docker-stats'

        thr = self.ProfilingThread(self.provider, probe, container_id)
        self._active_jobs[container_id] = {'thread': thr, 'tags': tags}
        thr.start()
        logger.debug('Started K8S Docker Stats profiling')
        return container_id

    def end_profiling(self, profile_job_id):
        if not profile_job_id or profile_job_id not in self._active_jobs:
            logger.warning('Stopping profiling job: Profiling job %s not found!', profile_job_id)
        job = self._active_jobs[profile_job_id]
        thr = job['thread']
        thr.stop()
        logger.debug('Stopped Docker Stats profiling')
        self._active_jobs.pop(profile_job_id, None)
        return DockerStatsProfiler.stats_to_profile(thr.data, job['tags'])

    def done_for_profiling(self, **kwargs):
        pod_name = kwargs['pod'].metadata.name
        k8s_node = kwargs['pod'].spec.node_name

        if k8s_node in self.probes and pod_name in self.probes[k8s_node]['users']:
            self.probes[k8s_node]['users'].remove(pod_name)
            logger.debug('probe on node %s used by %s', k8s_node, self.probes[k8s_node]['users'])
            if len(self.probes[k8s_node]['users']) == 0 and not self.KEEP_PROBES:
                logger.info('Removing profiling probe %s because it is not used anymore and KEEP_PROBES=false', self.probes[k8s_node]['job_name'])
                self.provider.delete_job(self.probes[k8s_node]['job_name'], self.probes[k8s_node]['namespace'])
                del self.probes[k8s_node]

    def shutdown(self):
        pass
