#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import traceback

import requests

from benchsuite.core.model.profilers import Profiler

logger = logging.getLogger(__name__)


class PledgerAppProfilerProfiler(Profiler):

    def __init__(self, provider):
        if 'appprofiler_base_url' not in provider.props:
            self.base_url = None
            logger.error('Cannot instantiate PledgerAppProfiler profiler: missing provider prop "appprofiler_base_url"')
        else:
            self.base_url = provider.props['appprofiler_base_url']

    def prepare_for_profiling(self, **kwargs):
        pass

    def start_profiling(self, profile_tags, **kwargs):
        if not profile_tags['sut']:
            logger.debug('Not calling APPProfiler because this is not a sut job')
            return

        if not self.base_url:
            logger.error('Base URL not defined. Cannot send profiling starting notification')
            return

        url = f'{self.base_url}/{kwargs["container_id"]}/{profile_tags["wid"]}'
        logger.info('Sending notification to the AppProfiler: %s', url)
        try:
            requests.get(url)
        except Exception as ex:
            logger.error('Error calling APPProfiler: %s', str(ex))
            print(traceback.format_exc())

    def end_profiling(self, profile_job_id):
        pass

    def done_for_profiling(self, **kwargs):
        pass

