#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging
import time
import traceback
import uuid

import requests

from benchsuite.core.model.profilers import Profiler

logger = logging.getLogger(__name__)


class KubernetesPrometheusProfiler(Profiler):

    def __init__(self, provider):
        self.prom_url = provider.prometheus_endpoint
        self._jobs = {}
        self.sampling_interval = 0.5  # seconds
        self.units = {
            "container_cpu_usage_seconds_total": 's',
            "container_memory_usage_bytes": 'bytes'
        }

    def prepare_for_profiling(self, **kwargs):
        pass

    def done_for_profiling(self, **kwargs):
        pass

    def start_profiling(self, profile_tags, **kwargs):
        client_time = time.time()
        response = requests.get(f'{self.prom_url}/api/v1/query?query=time()')

        server_time = response.json()['data']['result'][0]
        time_diff = server_time - client_time
        logger.debug('time diff between here and prometheus server is about: %ss', time_diff)
        pod_name = kwargs['pod_name']
        namespace = kwargs['pod_namespace']

        tags = dict(profile_tags)
        tags['profiler'] = 'k8s-prometheus'

        id = str(uuid.uuid4())
        self._jobs[id] = {'time_diff': time_diff, 'pod_name': pod_name, 'pod_namespace': namespace, 'start_time': client_time, 'tags': tags}

        return id

    def end_profiling(self, profile_job_id):
        now_time = time.time()

        pod_name = self._jobs[profile_job_id]['pod_name']
        namespace = self._jobs[profile_job_id]['pod_namespace']
        start_time = self._jobs[profile_job_id]['start_time'] + self._jobs[profile_job_id]['time_diff']
        end_time = now_time + self._jobs[profile_job_id]['time_diff']

        query = f'{self.prom_url}/api/v1/query_range?query={{__name__=~"{"|".join(list(self.units.keys()))}",pod="{pod_name}",namespace="{namespace}",container=""}}&start={start_time}&end={end_time}&step={self.sampling_interval}'
        logger.debug('Calling Prometheus: %s', query)

        response = requests.get(query)
        return self._query_response_to_profile(response.json(), self._jobs[profile_job_id]['tags'])

    def _query_response_to_profile(self, res, tags):
        profile = {}
        profile['t0'] = None
        profile['tags'] = tags
        profile['items'] = []
        profile['units'] = self.units
        try:
            t0 = res['data']['result'][0]['values'][0][0]
            profile['t0'] = t0

            metrics = {m['metric']['__name__']: m['values'] for m in res['data']['result']}
            metric_names = list(metrics.keys())

            for i in range(len(res['data']['result'][0]['values'])):
                timestamp = metrics[metric_names[0]][i][0] - t0
                values = {name: float(values[i][1]) for name, values in metrics.items()}
                values['time'] = int(timestamp * 1000)
                profile['items'].append(values)
        except Exception as ex:
            logger.error('Error occurred during parsing of prometheus response: %s', str(ex))
            logger.debug('Prometheus response was: %s', res)
            print(traceback.format_exc())
        return profile

