#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import re
import logging
import time
from contextlib import closing

from io import StringIO

import paramiko


#
# def get_data_dir():
#     d = user_data_dir('BenchmarkingSuite', None)
#     if not os.path.exists(d):
#         os.makedirs(d)
#     return d
#
# def get_executions_storage():
#     return get_data_dir() + os.path.sep + 'executions.dat'
#
#
# def get_environments_storage():
#     return get_data_dir() + os.path.sep + 'environments.dat'
from paramiko import RSAKey


logger = logging.getLogger(__name__)


# source: https://stackoverflow.com/questions/8641463/how-to-set-password-on-first-call-for-command-from-paramiko
def change_expired_password_over_ssh(host, username, current_password, new_password,
                                     server_questions=None):

    if server_questions is None:
        server_questions = [
            'Current password: ',
            'New password: ',
            'Retype new password: ',
            'root@localhost:~# ']

    def wait_until_channel_endswith(channel, endswith, wait_in_seconds=15):
        timeout = time.time() + wait_in_seconds
        read_buffer = ''
        while not read_buffer.endswith(endswith):
            if channel.recv_ready():
                read_buffer += channel.recv(4096).decode("utf-8")
                logger.debug(f'Read from channel:{read_buffer}')
            elif time.time() > timeout:
                raise TimeoutError(f"Timeout while waiting for '{endswith}' on the channel")
            else:
                time.sleep(1)

    with closing(paramiko.SSHClient()) as ssh_connection:
        ssh_connection.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh_connection.connect(hostname=host, username=username, password=current_password)
        ssh_channel = ssh_connection.invoke_shell()

        wait_until_channel_endswith(ssh_channel, server_questions[0])
        ssh_channel.send(f'{current_password}\n')

        wait_until_channel_endswith(ssh_channel, server_questions[1])
        ssh_channel.send(f'{new_password}\n')

        wait_until_channel_endswith(ssh_channel, server_questions[2])
        ssh_channel.send(f'{new_password}\n')
        wait_until_channel_endswith(ssh_channel, server_questions[3])


def ssh_transfer_output(vm, name, dest):
    out = '/tmp/' + name + '.out'
    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname=vm.ip, port=22, username=vm.username, key_filename=vm.keyfile)
        sftp = ssh.open_sftp()
        sftp.get(out, dest)
    finally:
        ssh.close()


def run_ssh_cmd(vm, cmd, _async=False, needs_pty=False, retry_times=3):

    retry_counter = retry_times

    while retry_counter > 0:
        retry_counter -= 1

        try:
            return run_ssh_cmd_single(vm, cmd, _async=_async, needs_pty=needs_pty)

        except Exception as ex:
            if retry_counter > 0:
                logger.warning('An exception occurred in the execution of the ssh command: {0}. Retrying for other {1} times'.format(str(ex), retry_counter))
            else:
                logger.warning('An exception occurred in the execution of the ssh command: {0}. '
                               'Not retrying because max retry times ({1}) exceeded. Raising the exeception'.format(str(ex), retry_times))
                raise ex


def run_ssh_cmd_with_client(ssh_client, cmd, env_dict={}):
    stdin, stdout, stderr = ssh_client.exec_command(cmd, environment=env_dict, get_pty=False)
    out = sanitize_output(stdout.read().decode("utf-8"))
    err = sanitize_output(stderr.read().decode("utf-8"))

    exit_status = stdout.channel.recv_exit_status()

    return exit_status, out, err


def run_ssh_cmd_single(vm, cmd, _async=False, needs_pty=False):
    '''
    sometime /etc/sudoers is configured to require a tty to execute a command with sudo. In this case, set needs_pty to
    True. But if needs_pty is True, you cannot run a command asyncrounously (check if this is really true)
    
    Defaults    !requiretty

    If aysnc is true, return code is 0 and stdout and stderr are both empty strings. That's because the function
    returns before they are available

    :param vm: 
    :param cmd: 
    :param needs_pty: 
    :return: 
    '''

    try:
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        if vm.priv_key:
            pkey = RSAKey.from_private_key(StringIO(vm.priv_key))  # assuming it is an RSAKey
            ssh.connect(hostname=vm.ip, port=vm.ssh_port, username=vm.username, pkey=pkey)
        else:
            ssh.connect(hostname=vm.ip, port=vm.ssh_port, username=vm.username, password=vm.password)

        logger.debug('Executing command on the remote host {0}: {1}'.format(vm.id, cmd))
        stdin, stdout, stderr = ssh.exec_command(cmd, get_pty=needs_pty)

        if _async:
            return (0, '', '')

        out = sanitize_output(stdout.read().decode("utf-8"))
        err = sanitize_output(stderr.read().decode("utf-8"))

        exit_status = stdout.channel.recv_exit_status()

        return (exit_status, out, err)

        # chan = ssh.get_transport().open_session()
        # chan.get_pty()
        # chan.exec_command(cmd)
        # print(chan.recv(1024))

    finally:
        ssh.close()


def sanitize_output(string):
    # remove ansi escape sequences
    ansi_escape = re.compile(r'(\x9B|\x1B\[)[0-?]*[ -\/]*[@-~]')
    return ansi_escape.sub('', string)
