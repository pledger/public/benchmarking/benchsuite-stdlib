#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import time

from pyvcloud.vcd.client import Client, BasicLoginCredentials, EntityType
from pyvcloud.vcd.firewall_rule import FirewallRule
from pyvcloud.vcd.gateway import Gateway
from pyvcloud.vcd.nat_rule import NatRule
from pyvcloud.vcd.org import Org
from pyvcloud.vcd.vapp import VApp
from pyvcloud.vcd.vdc import VDC
from pyvcloud.vcd.vm import VM

logger = logging.getLogger(__name__)


class PyVCloudHelper:

    def __init__(self, host, vdc, organization, user, password, verify_ssl_certs=True, edge_gateway=None, refresh_login=10 * 60):
        self.host = host
        self.vdc = vdc
        self.organization = organization
        self.user = user
        self.password = password
        self.verify_ssl_certs = verify_ssl_certs
        self.edge_gateway = edge_gateway
        self.refresh_login = refresh_login
        self.__client_cache = None
        self.__client_timestamp = 0

    def check_connection(self):
        self.__get_client()

    def delete_vapp(self, name):
        vdc = self.__get_vdc()
        vdc.delete_vapp(name=name, force=True)

    def get_catalogs(self, filter=None):
        catalogs = self.__get_org().list_catalogs()
        # TODO: filter using catalog name filter provided by the user in the config
        return catalogs

    def get_templates(self, catalog_name, filter=None):
        templates = self.__get_org().list_catalog_items(catalog_name)
        # TODO: filter using template name filter provided by the user in the config
        return templates

    def get_networks(self):
        return self.__get_vdc().list_orgvdc_routed_networks()

    def clean_all(self):
        vdc = self.__get_vdc()
        benchsuite_vapps = [vapp['name'] for vapp in vdc.list_resources(EntityType.VAPP) if
                            "benchsuite" in vapp['name']]
        for i in benchsuite_vapps:
            self.delete_vapp(i)

        self.delete_nat_rule('benchsuite')
        self.delete_firewall_rules('benchsuite')

    def create_vapp(self, name, catalog, template, vm_name, ncpus, memory, disk, network, network_adapter):
        vdc = self.__get_vdc()
        vapp_resource = vdc.instantiate_vapp(
            name,
            catalog,
            template,
            vm_name=vm_name,
            cpu=ncpus,
            memory=memory,
            disk_size=disk,
            network=network,
            network_adapter_type=network_adapter)
        vapp = VApp(vdc.client, resource=vapp_resource)
        power_state = vapp.get_power_state()
        retries = 10
        while power_state != 8 and retries > 0:
            logger.debug(f"Waiting 10 seconds for vapp {name} to be created (current status is {power_state}) (retires={retries})")
            time.sleep(10)
            try:
                vapp.reload()
            except Exception as ex:
                logger.error(f"Got an exception {ex}")
            power_state = vapp.get_power_state()
            retries -= 1
        logger.debug(f"Vapp {name} created")

        # Wait for the VM to be running
        vm = VM(vdc.client, resource=vapp.get_vm(vm_name))
        vm_power_state = vm.get_power_state()
        retries = 10
        while vm_power_state != 4 and retries > 0:
            logger.debug(f"Waiting 10 seconds for the VM being started (current status is {vm_power_state}) (retires={retries})")
            time.sleep(10)
            try:
                vm.reload()
            except Exception as ex:
                logger.error(f"Got an exception {ex}")
            vm_power_state = vm.get_power_state()
            retries -= 1
        logger.info(f"VM {vm_name} powered on")

        # Wait for the VM to acquire an ip address
        retries = 10
        while ("ip_address" not in vm.list_nics()[0]) and retries > 0:
            logger.debug(f"Waiting 30 seconds for the VM {vm_name} to acquire an ip address (retries={retries})")
            time.sleep(30)
            try:
              vm.reload()
            except Exception as ex:
                logger.error(f"Got an exception {ex}")
            retries -= 1
        return vm

    def get_vm_ip(self, vm):
        return vm.list_nics()[0]['ip_address']

    def create_nat_rule(self, type, original_ip, translated_ip, benchsuite_key):
        gw = self.__get_edge_gateway()
        logger.debug(f'Creating {type} rule (original: {original_ip}, translated: {translated_ip})')
        gw.add_nat_rule(type,
            original_address=original_ip,
            translated_address=translated_ip,
            description=f'{benchsuite_key}')

    def delete_nat_rule(self, benchsuite_key):
        gw = self.__get_edge_gateway()
        for rule in gw.get_nat_rules().natRules.natRule:
            if hasattr(rule, "description") and benchsuite_key in rule.description.text:
                nat_rule = NatRule(gw.client, gateway_name=gw.name, rule_id=rule.ruleId)
                logger.debug(f"Deleting nat rule {nat_rule.rule_id}")
                nat_rule.delete_nat_rule()

    def create_firewall_rule(self, source_ip, vm_name, public_ip):
        gateway = self.__get_edge_gateway()

        # Ingress rule
        name_in = f'{vm_name}-in'
        logger.debug(f'Creating firewall rule {name_in}: source: {source_ip}, destination: {public_ip}, port: any:22')
        gateway.add_firewall_rule(name_in)
        rule_in = FirewallRule(gateway.client, gateway.name,
                            [r['ID'] for r in gateway.get_firewall_rules_list() if r['name'].text == name_in][0])
        rule_in.edit(
            source_values=[f'{source_ip}:ip'],
            destination_values=[f'{public_ip}:ip'],
            services=[{'tcp': {'any': '22'}}])

        # Egress rule
        name_out = f'{vm_name}-out'
        logger.debug(f'Creating firewall rule {name_out}: source: {vm_name}, destination: any, port: any')
        gateway.add_firewall_rule(name_out)
        rule_out = FirewallRule(gateway.client, gateway.name,
                            [r['ID'] for r in gateway.get_firewall_rules_list() if r['name'].text == name_out][0])
        rule_out.edit(source_values=[f'{vm_name}:virtualmachine'])

    def delete_firewall_rules(self, name_prefix):
        gateway = self.__get_edge_gateway()
        rules = [r for r in gateway.get_firewall_rules_list() if r['name'].text.startswith(name_prefix)]
        for r in rules:
            logger.debug(f'Deleting firewall rule {r["name"]} {r}')
            rule = FirewallRule(gateway.client, gateway.name, r['ID'])
            rule.delete()

    def get_available_public_ip(self):
        gw = self.__get_edge_gateway()
        ips = gw.list_external_network_ip_allocations()

        # TODO: find a free ip (not used in nata tables)

        # FIXME: return the first ip of the first external network
        return ips[list(ips.keys())[0]][0]

    def __get_client(self):
        if (not self.__client_cache) or (time.time() - self.__client_timestamp > self.refresh_login):
            logger.debug(f'Logging in to {self.host}')
            self.__client_cache = Client(self.host, verify_ssl_certs=self.verify_ssl_certs)
            self.__client_cache.set_highest_supported_version()
            self.__client_cache.set_credentials(
                BasicLoginCredentials(self.user, self.organization, self.password))
            self.__client_timestamp = time.time()
        return self.__client_cache

    def __get_org(self):
        client = self.__get_client()
        return Org(client, resource=client.get_org())

    def __get_vdc(self):
        org = self.__get_org()
        return VDC(org.client, resource=org.get_vdc(self.vdc))

    def __get_edge_gateway(self) -> Gateway:
        vdc = self.__get_vdc()
        gateways = vdc.list_edge_gateways()

        gw_name = self.edge_gateway
        if not gw_name:
            if len(gateways) != 1:
                raise ValueError("If the edge_gateway parameter is not specified, exactly one gateway must exist")
            gw_name = gateways[0]['name']

        gw = [Gateway(vdc.client, name=g['name'], href=g['href']) for g in gateways if g['name'] == gw_name]
        if len(gw) != 1:
            raise ValueError(f"Zero or multiple edge gateways with name {gw_name}")
        return gw[0]