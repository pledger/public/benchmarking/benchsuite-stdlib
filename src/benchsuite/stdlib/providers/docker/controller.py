#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging
import time

import docker
from dateutil.parser import isoparse

from benchsuite.core.loaders.profilers import load_profiler
from benchsuite.core.model.profilers import NOOPProfiler
from benchsuite.core.model.provider import Provider
from benchsuite.stdlib.providers.docker.model import DockerProviderModel
from benchsuite.stdlib.providers.utils import ProviderTmpFileMixin, ProviderSSHTunnelingMixin

logger = logging.getLogger(__name__)


class DockerProviderController(Provider, ProviderTmpFileMixin, ProviderSSHTunnelingMixin):
    TYPE = 'docker'

    def __init__(self, model):
        super().__init__(model)
        self._client = None
        self._created_containers = []
        self._created_networks = []
        self._profiler = NOOPProfiler(None)
        self._node_name = None

    def override_props(self, props):
        # override props blindly
        if props:
            self.shutdown()
            self.__dict__.update(props)
            self.startup()

    def startup(self):
        super().startup()

        self._client = None

        if not self._client:
            tls_config = None
            if self.model.auth:
                client_cert_file = self._create_temp_file(self.model.auth.client_cert)
                client_key_file = self._create_temp_file(self.model.auth.client_key)
                ca_cert_file = self._create_temp_file(self.model.auth.ca_cert)
                tls_config = docker.tls.TLSConfig(verify=True, ca_cert=ca_cert_file,
                                                  client_cert=(client_cert_file, client_key_file))

            if self.model.base_url:
                url = self.model.base_url
            else:
                url = 'tcp://' + self._ssh_tunnel_local_binds['ssh_forwarded_server'][0] + ':' + str(
                    self._ssh_tunnel_local_binds['ssh_forwarded_server'][1])

            self._client = docker.DockerClient(base_url=url, tls=tls_config)

        if self.model.profiler:
            self._profiler = load_profiler(self.model.profiler, self)

        try:
            logger.debug('Trying to detect node name running a container to read the /etc/hostname file in the host')
            c_out = self._client.containers.run("alpine", 'cat /tmp/hostname',
                                                volumes={'/etc/hostname': {'bind': '/tmp/hostname', 'mode': 'ro'}},
                                                remove=True)
            self._node_name = c_out.decode().strip()
            logger.info('Detection of node name for Docker Engine successful. Node name is: %s', self._node_name)
            logger.warning('Node name detection only works for single node Docker Engines. Not tested with a Docker '
                           'cluster yet')
        except Exception as ex:
            logger.warning('Failed to get node name for the Docker Engine: %s', str(ex))

    def check_connection(self):
        res = self._get_client().info()
        logger.debug('check connection call returned %s', res)

    def shutdown(self):
        if self._client:
            self._client.close()
            self._client = None
        self._profiler = NOOPProfiler(None)
        super().shutdown()

    def _get_client(self):
        if not self._client:
            self.startup()
        return self._client

    def create_executions(self, session, benchmark):
        return [session.new_execution(benchmark)]

    def get_execution_environment(self, request):
        return None

    def generate_execution_params(self, param_combination_set):
        return [{}]

    def list_containers(self):
        return self._get_client().containers.list()

    def create_container(self, image, entrypoint=None, command=None, network=None, name=None, environment={},
                         volume_mounts={}, security_opts={}, labels={}):
        self._get_client().images.pull(image)

        volumes = {}
        for vm in volume_mounts:
            volumes[vm['path']] = {'bind': vm['mountPath'], 'mode': 'ro' if vm['readOnly'] else 'rw'}

        add_args = {'name': name, 'entrypoint': entrypoint, 'environment': environment,
                    'volumes': volumes,
                    'labels': {'managed-by': 'benchsuite', **labels}}

        if security_opts['capabilities']:
            add_args['cap_add'] = security_opts['capabilities']
        if security_opts['enableHostNetwork']:
            add_args['network_mode'] = 'host'
        if security_opts['enableHostPID']:
            add_args['pid_mode'] = 'host'
        if security_opts['enableHostUserNS']:
            add_args['userns_mode'] = 'host'

        container = self._get_client().containers.create(image, command=command, **add_args)
        self._created_containers.append(container.id)

        if network and not security_opts['enableHostNetwork']:
            if isinstance(network, str):
                network = self._get_client().networks.get(network)
            network.connect(container)

        return container.id

    def start_container(self, container, wait_until_running=False):
        if isinstance(container, str):
            container = self._get_client().containers.get(container)

        container.start()
        if wait_until_running:

            # wait few seconds because the container could go from "create" to "running" for few moments before fail
            # (e.g. if the process started into the container crashes)
            time.sleep(5)

            timeout = 120
            stop_time = 1
            elapsed_time = 0
            container.reload()
            while container.status == 'created' and elapsed_time < timeout:
                logger.debug('waiting for container %s to be "running". Current status is "%s"', container.id,
                             container.status)
                time.sleep(stop_time)
                elapsed_time += stop_time
                container.reload()
                continue
            logger.debug('container in status {0}'.format(container.status))
            return container.status == 'running'
        return True

    def wait_container(self, container):
        if isinstance(container, str):
            container = self._get_client().containers.get(container)

        return container.wait()['StatusCode']

    def get_container_status(self, container):
        logs = self.container_logs(container)
        ret_code = self.container_exit_code(container)
        container_runtime = self.container_runtime(container)
        return {
            'ret_val': ret_code,
            'runtime': container_runtime,
            'stdout': logs['stdout'],
            'stderr': logs['stderr'],
            'node': self._node_name
            # TODO: this could be set in the provider config (e.g. for Pledger it could be set from the node name in the Confservice)
        }

    def container_logs(self, container):
        if isinstance(container, str):
            container = self._get_client().containers.get(container)

        return {
            'stdout': container.logs(stderr=False).decode(),
            'stderr': container.logs(stdout=False).decode()
        }

    def container_runtime(self, container):
        if isinstance(container, str):
            container = self._get_client().containers.get(container)
        runtime = isoparse(container.attrs['State']['FinishedAt']) - isoparse(container.attrs['State']['StartedAt'])
        return runtime.total_seconds()

    def container_exit_code(self, container):
        if isinstance(container, str):
            container = self._get_client().containers.get(container)

        return container.attrs['State']['ExitCode']

    def wait_container_readiness(self, container, readiness_command):
        if isinstance(container, str):
            container = self._get_client().containers.get(container)

        timeout = 120
        stop_time = 5
        elapsed_time = 0
        logger.debug(f'Checking container {container.name} readiness with command {readiness_command}')
        res = container.exec_run(readiness_command)
        while res.exit_code != 0 and elapsed_time < timeout:
            logger.debug(f'retrying command because container was not ready ({res.exit_code})')
            time.sleep(stop_time)
            elapsed_time += stop_time
            res = container.exec_run(readiness_command)

        return res

    def container_ip(self, container, network=None):
        if isinstance(container, str):
            container = self._get_client().containers.get(container)
        if isinstance(network, str):
            network = self._get_client().networks.get(network)

        if network:
            return container.attrs['NetworkSettings']['Networks'][network.name]['IPAddress']
        else:
            return container.attrs['NetworkSettings']['IPAddress']

    def exec_container(self, container, cmd, redirect_to_logs=False):
        if isinstance(container, str):
            container = self._get_client().containers.get(container)

        if redirect_to_logs:
            cmd = cmd + ['>', '/proc/1/fd/1', '2>', '/proc/1/fd/2']

        logger.debug('Executing command "%s"', cmd)
        start = time.time()
        res = container.exec_run(cmd, demux=True)
        end = time.time() - start
        return {
            'runtime': int(end),
            'ret_val': res[0],
            'stdout': (res[1][0] or b'').decode(),
            'stderr': (res[1][1] or b'').decode(),
            'node': self._node_name
            # TODO: this could be set in the provider config (e.g. for Pledger it could be set from the node name in the Confservice)
        }

    def start_container_profiling(self, container, profile_tags):
        if isinstance(container, str):
            container = self._get_client().containers.get(container)
        return self._profiler.start_profiling(profile_tags, container_id=container.id)

    def stop_container_profiling(self, profiling_job_id):
        return self._profiler.end_profiling(profiling_job_id)

    def remove_container(self, container, stop_if_running=False, tracked=True):
        if isinstance(container, str):
            container = self._get_client().containers.get(container)

        if stop_if_running:
            container.reload()
            if container.status == 'running':
                logger.debug('Trying to stop running container %s before removing it', container.id)
                container.stop()
                logger.info('container %s stopped', container.id)

        container.remove(force=stop_if_running)
        logger.info('container %s removed', container.id)
        if tracked:
            self._created_containers.remove(container.id)

    def create_network(self, name, labels={}):
        network = self._get_client().networks.create(name, labels={'managed-by': 'benchsuite', **labels})
        self._created_networks.append(network.id)
        return network.id

    def remove_network(self, network, tracked=True):
        if isinstance(network, str):
            network = self._get_client().networks.get(network)

        network.remove()
        logger.debug('Networks %s removed', network.name)
        if tracked:
            self._created_networks.remove(network.id)

    def cleanup(self):
        # remove any existing container
        for cid in list(self._created_containers):
            self.remove_container(self._get_client().containers.get(cid), stop_if_running=True)
        # remove created networks
        for nid in list(self._created_networks):
            self.remove_network(nid)

        self.cleanup_mixins()

    def deep_cleanup(self, labels={}):

        # always use managed-by label
        labels['managed-by'] = 'benchsuite'

        label_filter = [f'{k}={v}' for k, v in labels.items()]

        logger.info('Deep cleaning: trying to delete all resources created by Benchsuite with labels %s', label_filter)

        deleted = 0
        for i in self._get_client().containers.list(all=True, filters={'label': label_filter}):
            self.remove_container(i, stop_if_running=True, tracked=False)
            deleted += 1

        for i in self._get_client().networks.list(filters={'label': label_filter}):
            self.remove_network(i, tracked=False)
            deleted += 1

        logger.info('Deleted %s resources', deleted)

    @staticmethod
    def load_from_dict(model):

        model = DockerProviderModel.parse_obj(model)

        dp = DockerProviderController(model)
        # dp.name = model.get('name', None)
        # dp.id = model.get('id', None)
        return dp
