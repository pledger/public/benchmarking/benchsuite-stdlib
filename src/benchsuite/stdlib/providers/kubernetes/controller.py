#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import json
import logging
import os
import time

import backoff
import kubernetes
import urllib3
from kubernetes.client import Configuration, V1PodStatus, V1JobStatus, V1VolumeMount, V1HostPathVolumeSource, V1Volume, \
    V1SecurityContext
from kubernetes.stream import stream
from kubernetes.stream.ws_client import STDOUT_CHANNEL, STDERR_CHANNEL, ERROR_CHANNEL

from benchsuite.core.loaders.profilers import load_profiler
from benchsuite.core.model.profilers import NOOPProfiler
from benchsuite.core.model.provider import Provider
from benchsuite.core.util.strings import random_id
from benchsuite.stdlib.providers.kubernetes.model import KubernetesProviderModel
from benchsuite.stdlib.providers.utils import ProviderTmpFileMixin, ProviderSSHTunnelingMixin

logger = logging.getLogger(__name__)

WATCH_OBJECT_TIMEOUT = 60 * 60
DEPLOYMENT_RUNNING_TIMEOUT = 60 * 5
POD_EXEC_CMD_TIMEOUT = 60 * 60


def api_fatal_exceptions(e):
    if isinstance(e, kubernetes.client.exceptions.ApiException):
        print('ApiException:\nstatus: {0}\nreason: {1}\nbody: {2}\nheaders: {3}\n'.format(
            e.status, e.reason, e.body, e.headers
        ))
        if e.status == 403:
            return True

        if 400 <= e.status < 600:

            return True

    return False


class ApiClientWithBackoff(kubernetes.client.ApiClient):

    MAX_TIME = 15 * 60  # 15 minutes

    @backoff.on_exception(backoff.expo, (urllib3.exceptions.MaxRetryError, kubernetes.client.exceptions.ApiException), max_time=MAX_TIME, giveup=api_fatal_exceptions)
    def call_api(self, resource_path, method, path_params=None, query_params=None, header_params=None, body=None,
                 post_params=None, files=None, response_type=None, auth_settings=None, async_req=None,
                 _return_http_data_only=None, collection_formats=None, _preload_content=True, _request_timeout=None,
                 _host=None):
        return super().call_api(resource_path, method, path_params, query_params, header_params, body, post_params,
                                files, response_type, auth_settings, async_req, _return_http_data_only,
                                collection_formats, _preload_content, _request_timeout, _host)


class KubernetesProviderController(Provider, ProviderTmpFileMixin, ProviderSSHTunnelingMixin):
    TYPE = 'kubernetes'

    def __init__(self, model):
        super().__init__(model)
        #self.namespace = self.model.namespace
        self.config = None
        self._profiler = NOOPProfiler(None)
        self.created_deployments = []
        self.created_services = []
        self.created_jobs = []
        self.keep_pods = False
        self.prometheus_endpoint = None
        #self.startup()

    def __getstate__(self):
        f = dict(self.__dict__)
        f.update({'config': None})
        return f

    def startup(self):
        logger.info('Setting up provider %s (id: %s)', self.name, self.id)
        super().startup()

        # build k8s config
        self.config = type.__call__(Configuration)

        if self.model.api_server_url:
            api_url = self.model.api_server_url
        elif self._ssh_tunnel_local_binds:
            api_url = 'https://' + self._ssh_tunnel_local_binds['ssh_forwarded_server'][0] + ':' + str(self._ssh_tunnel_local_binds['ssh_forwarded_server'][1])
        else:
            api_url = 'https://localhost:6443'

        self.config.host = api_url
        self.config.assert_hostname = False
        if self.model.host_cert:
            self.config.ssl_ca_cert = self._create_temp_file(self.model.host_cert)
        else:
            self.config.verify_ssl = False

        if self.model.user_token:
            self.config.api_key = {
                'authorization': 'Bearer {0}'.format(self.model.user_token)}

        if self.model.client_cert:
            self.config.cert_file = self._create_temp_file(self.model.client_cert)

        if self.model.client_key:
            self.config.key_file = self._create_temp_file(self.model.client_key)

        self.prometheus_endpoint = self.model.prometheus_endpoint

        if self.model.profiler:
            self._profiler = load_profiler(self.model.profiler, self)

    def get_core_v1(self):
        # NOTE: always return a new client because there are problems if the same client is used for
        # sync (e.g. get pods) and async (e.g. stream) operations
        # (an "APIException (0): Reason 200 ok" error is raised)
        if not self.config:
            self.startup()
        return kubernetes.client.CoreV1Api(ApiClientWithBackoff(self.config))

    def get_apps_v1(self):
        if not self.config:
            self.startup()
        return kubernetes.client.AppsV1Api(ApiClientWithBackoff(self.config))

    def get_batch_v1(self):
        if not self.config:
            self.startup()
        return kubernetes.client.BatchV1Api(ApiClientWithBackoff(self.config))

    def shutdown(self):
        super().shutdown()

    def pre_process_execution_params_combination(self, orig_params):
        res = dict(orig_params)
        if ('k8s_node' in res) and res['k8s_node'] == 'autodiscovered':
            res['k8s_node'] = self.__get_cluster_nodes()

        return res

    def __get_cluster_nodes(self):
        logger.debug('Contacting Kubernetes to get the list of nodes')
        try:
            v1 = self.get_core_v1()
            nodes = v1.list_node()
            all_nodes = [p for p in nodes.items]
            # remove master node (assuming it is not schedulable)
            filtered_nodes = []
            for n in all_nodes:
                taints = n.spec.taints
                ignore = False
                if taints:
                    print(taints)
                    for t in taints:
                        print(t.key)
                        if t.key == 'node-role.kubernetes.io/master':
                            ignore = True
                if not ignore:
                    filtered_nodes.append(n.metadata.labels['kubernetes.io/hostname'])
            return filtered_nodes
        except Exception as ex:
            logger.error('Error retrieving list of nodes: {0}'.format(ex))
            return []

    @backoff.on_exception(backoff.expo, (urllib3.exceptions.ProtocolError), max_time=15 * 60)
    def watch(self, func, namespace, selector, stop_condition, timeout=WATCH_OBJECT_TIMEOUT):

        def event_summary(evt):
            res = {'type': evt['type']}
            if 'object' in evt:
                if isinstance(evt['object'].status, V1PodStatus):
                    res['phase'] = evt['object'].status.phase
                elif isinstance(evt['object'].status, V1JobStatus):
                    res['active'] = evt['object'].status.active
                    res['failed'] = evt['object'].status.failed
                    res['succeeded'] = evt['object'].status.succeeded
            return res

        logger.debug('Watching %s "%s" in namespace "%s"', func.__name__, selector, namespace)
        w = kubernetes.watch.Watch()
        for evt in w.stream(func=func, namespace=namespace,
                            label_selector=selector, timeout_seconds=timeout):
            logger.debug('Received event: %s', event_summary(evt))
            if stop_condition(evt):
                w.stop()
                return {'last_event': evt, 'condition_matched': True}

        return {'last_event': evt, 'condition_matched': False}

    def create_deployment(self, image, namespace, name=None, cmd=None, args=None, env={}, readiness_cmd=None, node_selector=None, needs_profiling=False, security_opts={}, volume_mounts=[], labels={}):
        deployment_name = name if name else random_id()

        container = {
            'name': 'main',
            'image': image,
            'cmd': cmd,
            'args': args,
            'env': env,
            'volume_mounts': volume_mounts,
            'readiness_cmd': readiness_cmd
        }

        template = self.__create_pod_template([container], node_selector=node_selector,  labels={'bsname': deployment_name, 'managed-by': 'benchsuite', **labels}, security=security_opts)

        # Create the specification of deployment
        spec = kubernetes.client.V1DeploymentSpec(template=template, selector={
                "matchLabels":
                    {"bsname": deployment_name}})

        # Instantiate the deployment object
        deployment = kubernetes.client.V1Deployment(
            api_version="apps/v1",
            kind="Deployment",
            metadata=kubernetes.client.V1ObjectMeta(name=deployment_name, labels={'bsname': deployment_name, 'managed-by': 'benchsuite', **labels}),
            spec=spec,
        )
        appsV1 = self.get_apps_v1()
        resp = appsV1.create_namespaced_deployment(namespace, deployment)

        self.created_deployments.append((deployment_name, namespace))

        # wait for the pod to be running and with an ip assigned
        DEPLOYMENT_STATUS_READY_CONDITION = lambda evt: evt['object'] and evt['object'].status.container_statuses and (
                    evt['object'].status.container_statuses[0].ready) and \
                                                        evt['object'].status.pod_ip

        logger.debug('Waiting deployment in Ready status or failed')
        ext = self.watch(self.get_core_v1().list_namespaced_pod, namespace, "bsname={0}".format(deployment_name), DEPLOYMENT_STATUS_READY_CONDITION, timeout=DEPLOYMENT_RUNNING_TIMEOUT)
        if ext['condition_matched']:
            ip = ext['last_event']['object'].status.pod_ip
            status = 0
        else:
            ip = None
            status = 1

        if needs_profiling:
            pod = self.get_core_v1().list_namespaced_pod(label_selector="bsname={0}".format(deployment_name), namespace=namespace).items[0]
            self._profiler.prepare_for_profiling(pod=pod)

        return {'bsname': deployment_name, 'ip': ip, 'status': status}

    def create_service(self, name, namespace, cluster_ip, selector, port, targetPort, protocol, labels={}):
        template = self._create_svc_template(name, cluster_ip, selector, port, targetPort, protocol, labels={'managed-by': 'benchsuite', **labels})
        v1 = self.get_core_v1()

        v1.create_namespaced_service(namespace, template)

        self.created_services.append((name, namespace))

    def create_job(self, image, namespace, name=None, cmd=None, args=None, env={}, readiness_cmd=None, node_selector=None, restart_policy="Never", needs_profiling=False, user_id=None, volume_mounts=[], security_opts={}, labels={}):
        job_name = name if name else random_id()

        container = {
            'name': 'main',
            'image': image,
            'cmd': cmd,
            'args': args,
            'env': env,
            'readiness_cmd': readiness_cmd,
            'volume_mounts': volume_mounts,
            'user_id': user_id
        }

        init_containers = []
        if needs_profiling:
            init_containers.append({
                'name': 'bs-init',
                'image': 'curlimages/curl',
                'cmd': '/bin/sh',
                'args': ['-c', f'curl -s -G -X POST --unix-socket /var/run/docker.sock http://docker/images/create?fromImage={image} && until [ -f "/tmp/can_start" ]; do echo waiting for command to start; sleep 1; done'],
                'volume_mounts':  [{'name': 'docker-socket', 'type': 'hostPath', 'path': '/var/run/docker.sock', 'mountPath': '/var/run/docker.sock'}],
                'user_id': 0
            })

        template = self.__create_pod_template([container], init_containers=init_containers, node_selector=node_selector, restart_policy=restart_policy, labels={'managed-by': 'benchsuite', **labels}, security=security_opts)

        spec = kubernetes.client.V1JobSpec(
            template=template,
            backoff_limit=0)
        job = kubernetes.client.V1Job(
            api_version="batch/v1",
            kind="Job",
            metadata=kubernetes.client.V1ObjectMeta(name=job_name, labels={'managed-by': 'benchsuite', 'bsname': job_name, **labels}),
            spec=spec)

        client = self.get_batch_v1()
        client.create_namespaced_job(namespace, job)
        self.created_jobs.append((job_name, namespace))

        v1 = self.get_core_v1()

        if needs_profiling:
            logger.debug('Waiting pod init container running')
            evt = self.watch(self.get_core_v1().list_namespaced_pod, namespace, "job-name={0}".format(job_name),
                             lambda evt: evt['object'] and evt['object'].status.init_container_statuses and
                                         evt['object'].status.init_container_statuses[0].state.running)

            pod = v1.list_namespaced_pod(label_selector="job-name={0}".format(job_name), namespace=namespace).items[0]
            self._profiler.prepare_for_profiling(pod=pod)
            self.pod_exec_cmd(pod.metadata.name, namespace, ['touch', '/tmp/can_start'], 'bs-init')

        # wait for the job active or failed
        status = 0
        logger.debug('Waiting job running')
        ext = self.watch(self.get_core_v1().list_namespaced_pod, namespace, "job-name={0}".format(job_name),
                         lambda evt: (evt['object'] and (evt['object'].status.phase == 'Succeeded' or evt['object'].status.phase == 'Running' or evt['object'].status.phase == 'Failed')) or evt["type"] == "DELETED")

        if ext['condition_matched']:
            if ext['last_event']["type"] == "DELETED" or ext['last_event']["object"].status.phase == 'Failed':
                status = 1

        return {'bsname': job_name, 'status': status, 'pod_name': self.pod_name_from_job_name(job_name, namespace)}

    def wait_job(self, job_name, namespace):
        logger.debug('Waiting job completed')
        ext = self.watch(self.get_batch_v1().list_namespaced_job, namespace, "bsname={0}".format(job_name),
                         lambda evt: (evt['object'] and not evt['object'].status.active == 'Running') and (evt['object'].status.failed or evt['object'].status.succeeded))

        status = 0 if ext['condition_matched'] and ext['last_event']['object'].status.succeeded else 1

        return {'bsname': job_name, 'status': status}

    def pod_name_from_job_name(self, job_name, namespace):
        v1 = self.get_core_v1()
        pod = v1.list_namespaced_pod(label_selector="job-name={0}".format(job_name), namespace=namespace).items[0]
        return pod.metadata.name

    def job_exec_cmd(self, job_name, namespace, command, redirect_to_logs=False):
        if redirect_to_logs:
            command = command + ['>', '/proc/1/fd/1', '2>', '/proc/1/fd/2']
        pod_name = self.pod_name_from_job_name(job_name, namespace)
        return self.pod_exec_cmd(pod_name, namespace, command)

    def pod_exec_cmd(self, pod_name, namespace, command, container=None):
        logger.debug('Executing command %s on pod %s in namespace %s', command, pod_name, namespace)
        v1 = self.get_core_v1()
        client = stream(v1.connect_get_namespaced_pod_exec,
                      pod_name,
                      namespace,
                      container=container,
                      command=command,
                      stderr=True, stdin=False,
                      stdout=True, tty=False,
                      _preload_content=False)
        start = time.time()
        client.run_forever(timeout=POD_EXEC_CMD_TIMEOUT)
        end = time.time() - start
        stdout = client.read_channel(STDOUT_CHANNEL)
        stderr = client.read_channel(STDERR_CHANNEL)
        errch = json.loads(client.read_channel(ERROR_CHANNEL))

        # FIXME: use to retrieve the node where the pod is running. A more efficient way would be to get "pod"
        #        as argument and not "pod_name"
        pod = v1.list_namespaced_pod(namespace=namespace, field_selector=f'metadata.name={pod_name}').items[0]

        return {
            'runtime': int(end),
            'stdout': stdout,
            'stderr': stderr,
            'node': pod.spec.node_name,
            'ret_val': 0 if errch['status'] == 'Success' else int(errch['details']['causes'][0]['message'])
        }

    def delete_deployment(self, name, namespace, tracked=True):
        appsV1 = self.get_apps_v1()
        logger.debug('Deleting deployment %s', name)
        pod = self.pod_from_deployment_name(name, namespace)
        resp = appsV1.delete_namespaced_deployment(name, namespace)
        if tracked:
            self.created_deployments.remove((name, namespace))
        if self._profiler:
            self._profiler.done_for_profiling(pod=pod)

    def delete_service(self, name, namespace, tracked=True):
        v1 = self.get_core_v1()
        v1.delete_namespaced_service(name, namespace)
        if tracked:
            self.created_services.remove((name, namespace))

    def delete_job(self, job_name, namespace, tracked=True):
        client = self.get_batch_v1()
        v1 = self.get_core_v1()
        client.delete_namespaced_job(job_name, namespace=namespace)
        pod = v1.list_namespaced_pod(label_selector="job-name={0}".format(job_name), namespace=namespace).items[0]
        pod_name = pod.metadata.name
        v1.delete_namespaced_pod(pod_name, namespace=namespace)
        if tracked:
            self.created_jobs.remove((job_name, namespace))
        if self._profiler:
            self._profiler.done_for_profiling(pod=pod)

    def pod_from_deployment_name(self, deployment_name, namespace):
        v1 = self.get_core_v1()
        pod = v1.list_namespaced_pod(label_selector="bsname={0}".format(deployment_name), namespace=namespace).items[0]
        return pod

    def extract_pod_status(self, pod):
        # FIXME: this should be parametric
        container_pos = 0  # consider only the first container of the pod.

        container_status = pod.status.container_statuses[container_pos]
        if container_status.state.terminated:
            return {
                'ret_val': container_status.state.terminated.exit_code,
                'status': 'terminated',
                'message': container_status.state.terminated.message
            }
        if container_status.state.running:
            return {
                'ret_val': None,
                'status': 'running',
                'message': None
            }
        if container_status.last_state.terminated:
            return {
                'ret_val': container_status.last_state.terminated.exit_code,
                'status': 'terminated',
                'message': container_status.last_state.terminated.message
            }

    def get_deployment_status(self, deployment_name, namespace):
        pod = self.pod_from_deployment_name(deployment_name, namespace)
        node = pod.spec.node_name
        pod_name = pod.metadata.name
        v1 = self.get_core_v1()
        log = v1.read_namespaced_pod_log(pod_name, namespace=namespace)
        state = self.extract_pod_status(pod)
        return {'ret_val': state['ret_val'], 'stdout': log, 'stderr': state['message'] if state['ret_val'] else '', 'runtime': -1, 'node': node, 'status': state['status']}

    def get_job_status(self, job_id, namespace):
        v1 = self.get_core_v1()
        pod = v1.list_namespaced_pod(label_selector="job-name={0}".format(job_id), namespace=namespace).items[0]
        state = self.extract_pod_status(pod)
        pod_name = pod.metadata.name
        log = v1.read_namespaced_pod_log(pod_name, namespace=namespace)
        node = pod.spec.node_name
        client = self.get_batch_v1()
        res = client.list_namespaced_job(namespace=namespace, field_selector="metadata.name={0}".format(job_id))
        runtime = -1
        if not res.items[0].status.active and not res.items[0].status.failed:
            diff = res.items[0].status.completion_time - res.items[0].status.start_time
            runtime = diff.total_seconds()
        return {'ret_val': state['ret_val'], 'stdout': log, 'stderr': state['message'] if state['ret_val'] else '', 'runtime': runtime, 'node': node, 'status': state['status']}

    def cleanup(self):
        for d,n in list(self.created_deployments):
            self.delete_deployment(d, n)

        for j, n in list(self.created_jobs):
            self.delete_job(j, n)

        for s, n in list(self.created_services):
            self.delete_service(s, n)

    def deep_cleanup(self, labels={}):

        # always use managed-by label
        labels['managed-by'] = 'benchsuite'

        label_selector = ','.join([f'{k}={v}' for k,v in labels.items()])

        namespace = self.model.execution_params_combination['namespace']
        logger.info('Deep cleaning: trying to delete all resources created by Benchsuite on the provider in namespace "%s" with selector %s', namespace, label_selector)

        if isinstance(namespace, list):
            logger.error('Multiple namespaces specified, this is not supported by the deep cleanup function at the moment!')
            exit

        deleted = 0

        v1 = self.get_core_v1()
        for p in v1.list_namespaced_service(namespace=namespace, label_selector=label_selector).items:
            self.delete_service(p.metadata.name, namespace, tracked=False)
            deleted += 1

        batch = self.get_batch_v1()
        for p in batch.list_namespaced_job(namespace=namespace, label_selector=label_selector).items:
            self.delete_job(p.metadata.name, namespace, tracked=False)
            deleted += 1

        appsV1 = self.get_apps_v1()
        for p in appsV1.list_namespaced_deployment(namespace=namespace, label_selector=label_selector).items:
            self.delete_deployment(p.metadata.name, namespace, tracked=False)
            deleted += 1

        logger.info('Deleted %s resources', deleted)

    def start_job_profiling(self, name, namespace, profile_tags):
        v1 = self.get_core_v1()
        pod = v1.list_namespaced_pod(label_selector="job-name={0}".format(name), namespace=namespace).items[0]
        return self._start_pod_profiling(pod, namespace, profile_tags)

    def stop_job_profiling(self, profiling_job_id):
        return self._profiler.end_profiling(profiling_job_id)

    def start_deployment_profiling(self, name, namespace, profile_tags):
        v1 = self.get_core_v1()
        pod = v1.list_namespaced_pod(label_selector="bsname={0}".format(name), namespace=namespace).items[0]
        return self._start_pod_profiling(pod, namespace, profile_tags)

    def stop_deployment_profiling(self, profiling_job_id):
        return self._profiler.end_profiling(profiling_job_id)

    def _start_pod_profiling(self, pod, namespace, profile_tags):
        pod_name = pod.metadata.name
        pod_node = pod.spec.node_name
        return self._profiler.start_profiling(profile_tags, pod_name=pod_name, pod_node=pod_node, pod_namespace=namespace)

    def __create_container_template(self, image, name=None, cmd=None, args=None, env={}, readiness_cmd=None, volume_mounts=[], user_id=None):

        container_volume_mounts = []
        pod_volumes = []
        for vm in volume_mounts:
            if vm['type'] == 'hostPath':
                container_volume_mounts.append(V1VolumeMount(mount_path=vm['mountPath'], name=vm['name'], read_only=vm.get('readOnly', True)))
            # TODO: support other Volume Mount types

        k8s_env = [kubernetes.client.V1EnvVar(name=k, value=str(v)) for k, v in env.items()]
        container = kubernetes.client.V1Container(
            name=name if name else random_id(),
            image=image,
            env=k8s_env,
            command=[cmd] if cmd else None,
            args=args if args else None,
            readiness_probe=kubernetes.client.V1Probe(
                _exec=kubernetes.client.V1ExecAction(command=readiness_cmd),
                timeout_seconds=120) if readiness_cmd else None,
            volume_mounts=container_volume_mounts,
            security_context=None if user_id is None else V1SecurityContext(run_as_user=user_id)
        )

        return container

    def __create_pod_template(self, containers, init_containers=[], restart_policy=None, labels={}, node_selector=None, security={}):

        pod_volumes = []
        containers_templates = []
        init_containers_template = []
        for c in containers:
            containers_templates.append(self.__create_container_template(**c))
            if 'volume_mounts' in c:
                for vm in c['volume_mounts']:
                    if not [v for v in pod_volumes if v.name == vm['name']]:
                        if vm['type'] == 'hostPath':
                            pod_volumes.append(V1Volume(name=vm['name'], host_path=V1HostPathVolumeSource(path=vm['path'])))
                        # TODO: support other Volume Mount types

        for c in init_containers:
            init_containers_template.append(self.__create_container_template(**c))
            if 'volume_mounts' in c:
                for vm in c['volume_mounts']:
                    if not [v for v in pod_volumes if v.name == vm['name']]:
                        if vm['type'] == 'hostPath':
                            pod_volumes.append(V1Volume(name=vm['name'], host_path=V1HostPathVolumeSource(path=vm['path'])))
                        # TODO: support other Volume Mount types

        node_selector = {'kubernetes.io/hostname': node_selector} if node_selector else None

        # Create and configure a spec section
        template = kubernetes.client.V1PodTemplateSpec(
            metadata=kubernetes.client.V1ObjectMeta(labels=labels),
            spec=kubernetes.client.V1PodSpec(init_containers=init_containers_template,containers=containers_templates, restart_policy=restart_policy,
                                             node_selector=node_selector, volumes=pod_volumes, host_pid=security.get('host_pid', False), host_network=security.get('host_network', False)),
        )

        return template

    def _create_svc_template(self, name, cluster_ip, selector, port, targetPort, protocol, labels={}):
        v1 = kubernetes.client.CoreV1Api(kubernetes.client.ApiClient(self.config))
        template =  kubernetes.client.V1Service(
            metadata=kubernetes.client.V1ObjectMeta(name=name, labels=labels),
            spec=kubernetes.client.V1ServiceSpec(
                cluster_ip=cluster_ip,
                selector=selector,
                ports=[kubernetes.client.V1ServicePort(port=port, target_port=targetPort, protocol=protocol)])
        )
        return template

    def check_connection(self):
        res = kubernetes.client.CoreApi(kubernetes.client.ApiClient(self.config)).get_api_versions().versions
        logger.debug('Kubernetes Provider check_connection() was ok: %s', res)
        if self.model.execution_params_combination.get('k8s_node', None) == 'autodiscovered':
            logger.info('Checking if the user has permissions to list nodes (because autodiscovery is enabled)')
            res = kubernetes.client.CoreV1Api(kubernetes.client.ApiClient(self.config)).list_node()

    def destroy_service(self):
        if self.tunnel:
            self.tunnel.stop()
        for f in self._temp_files:
            os.remove(f)

    def get_provider_properties_dict(self):
        return {
            'id': self.name,
            'size': 'k8s',
            'image': 'k8s',
            'service_type': self.service_type,
            'metadata': self.metadata
        }

    @staticmethod
    def load_from_dict(dict):
        model = KubernetesProviderModel.parse_obj(dict)
        cp = KubernetesProviderController(model)
        return cp


