#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from typing import Optional

from pydantic import Extra, Field

from benchsuite.stdlib.providers.model import ProviderWithSSHTunnel


class KubernetesProviderModel(ProviderWithSSHTunnel, extra=Extra.forbid):
    driver: str = Field('kubernetes', const=True)
    api_server_url: Optional[str]
    host_cert: Optional[str]
    api_server_cert: Optional[str]
    auth_method: Optional[str]

    client_cert: Optional[str]
    client_key: Optional[str]
    user_token: Optional[str]
    prometheus_endpoint: Optional[str]
    profiler: Optional[str]
    #namespace: Optional[str]
    #nodes_selection_strategy: Optional[str] = None
    #nodes: Optional[Union[str, List[str]]] = None

    #_convert_workloads = validator('nodes', allow_reuse=True)(comma_separated_list)
