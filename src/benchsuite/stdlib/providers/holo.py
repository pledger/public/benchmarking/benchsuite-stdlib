#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import time
from io import StringIO
from typing import Optional

import paramiko
import requests
from paramiko.rsakey import RSAKey
from pydantic import Extra, Field, BaseModel

from benchsuite.core.model.provider import Provider, ProviderProvisioner
from benchsuite.stdlib.execenvs.docker import DockerExecutionEnvironment
from benchsuite.stdlib.execenvs.vmssh import VMSSHExecutionEnvironment
from benchsuite.stdlib.providers.docker.controller import DockerProviderController
from benchsuite.stdlib.providers.docker.model import DockerProviderModel
from benchsuite.stdlib.providers.libcloud.model import VM
from benchsuite.stdlib.providers.model import ProviderWithSSHTunnel, ProviderSSHTunnelingModel, SSHRemoteModel
from benchsuite.stdlib.providers.utils import ProviderTmpFileMixin, ProviderSSHTunnelingMixin
from benchsuite.stdlib.provisioners.openstack_docker import OpenstackDockerProvisioner
from benchsuite.stdlib.provisioners.openstack_ssh import OpenstackSSHProvisioner
from benchsuite.stdlib.utils.ssh import run_ssh_cmd_with_client

logger = logging.getLogger(__name__)


class HOLOVMModel(BaseModel, extra=Extra.forbid):
    ip: str
    user: str
    platform: str
    working_dir: str
    password: str
    ssh_port: str
    ssh_tunnel: Optional[ProviderSSHTunnelingModel]


class HoloProviderModel(ProviderWithSSHTunnel, extra=Extra.forbid):
    driver: str = Field('holo', const=True)
    api_url: str = Field(None)
    vm: HOLOVMModel


class HoloSSHProvisioner(OpenstackSSHProvisioner):
    SUPPORTED_PROVIDERS = ['holo']

    def provision(self, execution_params, execution_context, hard=True, dry_run=False):

        if dry_run:
            return VMSSHExecutionEnvironment(self.provider, VM('dummy-vm', '0.0.0.0', 'dummy-user', 'dummy-platform'))

        vm = self.provider.get_vm() if not hard else None  # try to re-use an existing vm if hard=False
        if not vm:
            metadata = {f'benchsuite.session.{k}': v for k, v in execution_context['props']['session'].items()}
            vm = self.provider.create_vm(execution_params['n_cpu'], execution_params['n_mem'])

        self.provisioned_vms = [vm]

        return VMSSHExecutionEnvironment(self.provider, vm, tags={
            'cpu': vm.metadata['cpu'],
            'memory': vm.metadata['memory']
        }, bsid_tags=['cpu', 'memory'])

    def ensure_connectivity(self, exec_env):

        vm = self.provisioned_vms[0]

        if f'{vm.id}-docker' not in self.provider._ssh_tunnel_local_binds:
            ip, port = self.provider.ensure_connection_to_vm(vm)

            self.provider._create_ssh_tunnels_from_config(ProviderSSHTunnelingModel(
                server=f'{ip}:{port}',
                user=vm.username,
                password=vm.password,
                remote=[SSHRemoteModel(name=f'{vm.id}-docker', server='127.0.0.1:2375')]))

            docker_port = self.provider._ssh_tunnel_local_binds[f'{vm.id}-docker']
            logger.info('Created SSH tunnel to access docker daemon %s', docker_port)

            exec_env._client.model.base_url = f'tcp://{docker_port[0]}:{docker_port[1]}'
            exec_env._client.startup()

    def deprovision(self, hard=True):
        if not self.provisioned_vms:
            return

        vm = self.provisioned_vms[0]

        if f'{vm.id}-docker' in self.provider._ssh_tunnel_local_binds:
            self.provider._remove_ssh_tunnel(f'{vm.id}-docker')

        if hard:
            self.provider.destroy_vm()


class HoloProvisioner(OpenstackDockerProvisioner):
    SUPPORTED_PROVIDERS = ['holo']

    def provision(self, execution_params, execution_context, hard=True, dry_run=False):

        if dry_run:
            return DockerExecutionEnvironment(None, {}, [])

        vm = self.provider.get_vm() if not hard else None  # try to re-use an existing vm if hard=False

        if not vm:
            metadata = {f'benchsuite.session.{k}': v for k, v in execution_context['props']['session'].items()}
            vm = self.provider.create_vm(execution_params['n_cpu'], execution_params['n_mem'])

        self.provisioned_vms = [vm]

        if not vm.metadata.get('docker_installed'):
            self._install_docker(vm)

        if f'{vm.id}-docker' not in self.provider._ssh_tunnel_local_binds:
            ip, port = self.provider.ensure_connection_to_vm(vm)

            self.provider._create_ssh_tunnels_from_config(ProviderSSHTunnelingModel(
                server=f'{ip}:{port}',
                user=vm.username,
                password=vm.password,
                remote=[SSHRemoteModel(name=f'{vm.id}-docker', server='127.0.0.1:2375')]))

        docker_port = self.provider._ssh_tunnel_local_binds[f'{vm.id}-docker']
        logger.info('Created SSH tunnel to access docker daemon %s', docker_port)

        model = DockerProviderModel(id='docker-engine', name='docker-engine', profiler='docker-stats', base_url=f'tcp://{docker_port[0]}:{docker_port[1]}')
        prov = DockerProviderController(model)
        return DockerExecutionEnvironment(prov, tags={
            'cpu': vm.metadata['cpu'],
            'memory': vm.metadata['memory']
        }, bsid_tags=['cpu', 'memory'])

    def ensure_connectivity(self, exec_env):

        vm = self.provisioned_vms[0]

        if f'{vm.id}-docker' not in self.provider._ssh_tunnel_local_binds:
            ip, port = self.provider.ensure_connection_to_vm(vm)

            self.provider._create_ssh_tunnels_from_config(ProviderSSHTunnelingModel(
                server=f'{ip}:{port}',
                user=vm.username,
                password=vm.password,
                remote=[SSHRemoteModel(name=f'{vm.id}-docker', server='127.0.0.1:2375')]))

            docker_port = self.provider._ssh_tunnel_local_binds[f'{vm.id}-docker']
            logger.info('Created SSH tunnel to access docker daemon %s', docker_port)

            exec_env._client.model.base_url = f'tcp://{docker_port[0]}:{docker_port[1]}'
            exec_env._client.startup()

    def deprovision(self, hard=True):
        if not self.provisioned_vms:
            return

        vm = self.provisioned_vms[0]

        if f'{vm.id}-docker' in self.provider._ssh_tunnel_local_binds:
            self.provider._remove_ssh_tunnel(f'{vm.id}-docker')

        if hard:
            self.provider.destroy_vm()


class HoloProviderController(Provider, ProviderTmpFileMixin, ProviderSSHTunnelingMixin):

    def __init__(self, model):
        super().__init__(model)
        self.api_url = None
        self.__vm = None

    def pre_process_execution_params_combination(self, orig_params):
        return dict(orig_params)

    def startup(self):
        self.startup_mixins()  # cannot use super().startup() here because the openstack method would be called

        self.api_url = self.model.api_url

        if self._ssh_tunnel_local_binds:
            self.api_url = f'http://{self._ssh_tunnel_local_binds["ssh_forwarded_server"][0]}:{self._ssh_tunnel_local_binds["ssh_forwarded_server"][1]}'

    def shutdown(self):
        self.shutdown_mixins()  # cannot use super().startup() here because the openstack method would be called

    def check_connection(self):
        pass

    def get_vm(self):
        return self.__vm

    def __execute_post_create(self, vm, retries):
        logger.info('Trying to connect to the new instance (max_retries={0})'.format(retries))
        try:
            with self.get_vm_ssh_client(vm) as ssh_client:
                run_ssh_cmd_with_client(ssh_client, 'echo "hello world!"')

        except Exception as ex:
            retries -= 1

            if retries > 0:
                period = 30
                logger.warning('Error connecting ({0}). The instance could be not ready yet. '
                               'Waiting {2} seconds and retry for {1} times'.format(str(ex), retries, period))
                time.sleep(int(period))
                self.__execute_post_create(vm, retries)
            else:
                logger.warning('Error connecting ({0}). Max number of retries achieved. Raising the exception'.format(str(ex)))
                raise ex

    def create_vm(self, n_cpu, n_mem):
        self.startup()
        url = f'{self.api_url}/benchmarking'
        payload = {'numberOfCpus': n_cpu, 'ramSize': n_mem}
        logger.info('Calling %s with payload %s', url, payload)
        res = requests.post(url, json=payload)
        logger.debug('HOLO API response: %s', res.text)
        if res.text == 'Ubuntu Instance Started!':
            self.__vm = VM('holo-testing-vm', self.model.vm.ip, self.model.vm.user, platform=self.model.vm.platform, working_dir=self.model.vm.working_dir, password=self.model.vm.password, ssh_port=self.model.vm.ssh_port, metadata={
                'docker_installed': True,
                'cpu': n_cpu, 'memory': n_mem})
            self.__execute_post_create(self.__vm, 20)
            logger.info('Waiting 120 seconds because if the VM has been just started, the Docker Engine could take some time to get running')
            time.sleep(120)
            return self.__vm
        else:
            raise Exception('Failed to create HOLO VM: %s', res.text)
            #self.__vm = VM('holo-testing-vm', self.model.vm.ip, self.model.vm.user, platform=self.model.vm.platform, working_dir=self.model.vm.working_dir, password=self.model.vm.password, ssh_port=self.model.vm.ssh_port, metadata={})
            #return self.__vm

    def ensure_connection_to_vm(self, vm):
        if self.model.vm.ssh_tunnel:
            if not 'vm-' + vm.id in self._ssh_tunnel_local_binds:
                tunnel_cfg = self.model.vm.ssh_tunnel.copy()
                self._create_ssh_tunnels_from_config(
                    tunnel_cfg,
                    last_tunnel_remote_default=[SSHRemoteModel(name='vm-' + vm.id, server=vm.ip)])
            return "localhost", self._ssh_tunnel_local_binds['vm-' + vm.id][1]
        return vm.ip, vm.ssh_port

    def get_vm_ssh_client(self, vm):

        if self.model.vm.ssh_tunnel:
            ip, port = self.ensure_connection_to_vm(vm)
        else:
            ip = vm.ip
            port = vm.ssh_port

        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        if vm.priv_key:
            pkey = RSAKey.from_private_key(StringIO(vm.priv_key))  # assuming it is an RSAKey
            ssh.connect(hostname=ip, port=port, username=vm.username, pkey=pkey)
        else:
            ssh.connect(hostname=ip, port=port, username=vm.username, password=vm.password)

        return ssh

    def destroy_vm(self):
        self.startup()
        url = f'{self.api_url}/stop'
        logger.info('Calling %s', url)
        res = requests.post(url)
        logger.debug('HOLO API response: %s', res.text)
        self.__vm = None
        logger.info('Waiting 30 seconds to ensure the VM has been stop')
        time.sleep(30)

    def ensure_connection_to_vm(self, vm):
        # copied from Openstack Provider Controller
        if self.model.vm.ssh_tunnel:
            if not 'vm-' + vm.id in self._ssh_tunnel_local_binds:
                tunnel_cfg = self.model.vm.ssh_tunnel.copy()
                self._create_ssh_tunnels_from_config(
                    tunnel_cfg,
                    last_tunnel_remote_default=[SSHRemoteModel(name='vm-' + vm.id, server=vm.ip)])
            return "localhost", self._ssh_tunnel_local_binds['vm-' + vm.id][1]
        return vm.ip, vm.ssh_port

    def cleanup(self):
        self.destroy_vm()

    def deep_cleanup(self, labels=None):
        self.cleanup()

    @staticmethod
    def load_from_dict(model):
        model = HoloProviderModel.parse_obj(model)
        return HoloProviderController(model)