#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from typing import Dict, Optional, Union, List, ForwardRef

from pydantic import BaseModel, Field, validator, Extra

from benchsuite.core.model.benchmark import BenchsuiteObject


class ProviderModel(BenchsuiteObject):

    id: str
    name: str
    profiler: str = None
    driver: str = None
    description: str = ''
    grants: Dict = {}
    metadata: Dict = {}
    props: Dict = {}
    execution_params_combination: Optional[Dict[str, Union[str, List]]]

    @property
    def bsid(self):
        return self.name


ProviderSSHTunnelingModel = ForwardRef('ProviderSSHTunnelingModel')


class SSHRemoteModel(BaseModel):
    name: str
    server: str


def remote_converter(val):
    if val is None:
        return None
    if isinstance(val, list):
        return val
    return [SSHRemoteModel(name='ssh_forwarded_server', server=val)]


class ProviderSSHTunnelingModel(BaseModel):
    server: str
    user: str
    password: str = Field(None)
    private_key: str = Field(None)
    port: int = Field(None)
    remote: Optional[Union[str, List[SSHRemoteModel]]]
    ssh_tunnel: Optional[ProviderSSHTunnelingModel]

    _convert_remote = validator('remote', allow_reuse=True)(remote_converter)


ProviderSSHTunnelingModel.update_forward_refs()


class ProviderWithSSHTunnel(ProviderModel, extra=Extra.forbid):
    ssh_tunnel: Optional[ProviderSSHTunnelingModel]
