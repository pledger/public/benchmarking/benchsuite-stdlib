#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from typing import Optional, Dict

from pydantic import Extra, BaseModel, Field

from benchsuite.stdlib.providers.model import ProviderSSHTunnelingModel, ProviderWithSSHTunnel


class VM:

    def __init__(self, id, ip, username, platform, working_dir=None, priv_key=None, password=None, ssh_port=22, metadata={}):
        self.ip = ip
        self.id = id
        self.username = username
        self.priv_key = priv_key
        self.platform = platform
        self.password = password
        self.working_dir = working_dir or ('/home/' + self.username if self.username else "/tmp")
        self.ssh_port = 22
        self.metadata = metadata

    def __str__(self) -> str:
        return "VM[ip: {0}]".format(self.ip)


class VMSConfModel(BaseModel, extra=Extra.forbid):
    network: Optional[str]
    security_group: Optional[str]
    no_floating_ip: bool = False
    platform: Dict[str, str] = {}
    username: Dict[str, str] = {}
    post_create_script: Dict[str, str] = {}
    ssh_tunnel: Optional[ProviderSSHTunnelingModel]


class LibcloudProviderModel(ProviderWithSSHTunnel, extra=Extra.forbid):
    driver: str = Field(None, const=True)
    access_id: str = None
    secret_key: str = None
    region: str = None
    network: str = None
    vms_conf: VMSConfModel = Field(VMSConfModel())


class AWSEC2ProviderModel(LibcloudProviderModel, extra=Extra.forbid):
    driver: str = Field('aws-ec2', const=True)


class OpenstackProviderModel(LibcloudProviderModel, extra=Extra.forbid):
    driver: str = Field('openstack', const=True)
    tenant: str = None
    domain: str = None
    auth_url: str = Field(None)
    auth_version: str = None