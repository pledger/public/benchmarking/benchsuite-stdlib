#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging
import time

from libcloud.compute.providers import get_driver

from benchsuite.core.util.strings import random_id
from benchsuite.stdlib.providers.libcloud.model import VM
from benchsuite.stdlib.utils.libcloud_helper import get_helper, guess_platform, guess_username, guess_working_dir

logger = logging.getLogger(__name__)


class AWSEC2Client:

    def __init__(self, access_id, secret_key, region, extra_args={}, platform_mappings={}, username_mappings={}):

        self.platform_mappings = platform_mappings
        self.username_mappings = username_mappings
        self.helper = get_helper('ec2')

        cls = get_driver('ec2')
        self._driver = cls(access_id, secret_key, region=region, **extra_args)

        self._images = None
        self._sizes = None

    def create_vm(self, size, image, network_name=None, security_group_name=None, assign_floating_ip=False, metadata={}):

        if not self._sizes:
            self._sizes = self._driver.list_sizes()
            logger.debug('Loaded {0} sizes'.format(len(self._sizes)))

        if not self._images:
            self._images = self._driver.list_images()
            logger.debug('Loaded {0} images'.format(len(self._images)))

        #  2. select the correct image and size
        try:
            size_obj = [s for s in self._sizes if s.id == size or s.name == size][0]
        except IndexError:
            logger.debug('Requested size %s not available. Aborting', size)
            raise Exception('Size {0} not available'.format(size))

        try:
            image_obj = [i for i in self._images if i.id == image or i.name == image][0]
        except IndexError:
            logger.debug('Requested image %s not available. Aborting', image)
            raise Exception('Image {0} not available'.format(image))

        logger.debug('Creating new Instance with image %s and size %s', image_obj.name, size_obj.name)

        #  3. choose a random name for the vm
        name = 'bsexec-' + random_id()

        key_name, ssh_private_key = self.helper.create_keypair(self._driver)

        extra_args = {'ex_metadata': metadata}
        extra_args.update(self.__get_newvm_network_param(network_name) or {})
        extra_args.update(self.__get_newvm_security_group_param(security_group_name) or {})

        logger.debug('Creating node with:')
        logger.debug(' - name: %s', str(name))
        logger.debug(' - image: %s', str(image_obj.name))
        logger.debug(' - size: %s', str(size_obj.id))
        logger.debug(' - keyname: %s', str(key_name))
        logger.debug(' - extra_args: %s', extra_args)
        node = self._driver.create_node(name=name, image=image_obj, size=size_obj, ex_keyname=key_name, **extra_args)

        # any exception occurring from this point, will delete the created VM,
        # to avoid to leave un-configured nodes running
        try :

            self._driver.wait_until_running([node], wait_period=10, ssh_interface='private_ips')

            #  4. refresh the info of the node
            node = [i for i in self._driver.list_nodes() if i.uuid == node.uuid][0]

            logger.debug('New Instance created with node_id=%s', node.id)

            # if the node has not public ips, try to assign one

            if assign_floating_ip and not node.public_ips:
                    ip = self.__assign_floating_ip(node)
                    if ip:
                        node.public_ips = [ip]

            if node.public_ips:
                vm_ip = node.public_ips[0]
            else:
                vm_ip = node.private_ips[0]

            platform = self.platform_mappings.get(image_obj.name, None)
            if not platform:
                platform = guess_platform(image_obj.name)
                logger.warning('"platform" not specified. Using "%s"', platform)

            username = self.username_mappings.get(image_obj.name, None)
            if not username:
                username = guess_username(platform)
                logger.warning('"username" not specified. Using "%s"', username)

            vm = VM(node.id, vm_ip, username, platform,
                    working_dir=guess_working_dir(username, platform),
                    priv_key=ssh_private_key,
                    metadata={'keypair_name': key_name, 'size': size, 'image': image})

            vm.metadata['cpu'] = size_obj.vcpus if hasattr(size_obj, 'vcpus') else (size_obj.extra['cpu'] if 'cpu' in size_obj.extra else 0)
            vm.metadata['memory'] = size_obj.ram
            vm.metadata['disk'] = size_obj.disk
            vm.metadata['flavour'] = size_obj.id
            vm.metadata['image'] = image_obj.name

            return vm

        except Exception as ex:
            logger.error('{0} occurred during VM initialization: {1}'.format(
                ex.__class__.__name__, str(ex)))
            logger.error('Destroying VM due to the initialization errors')
            # destroying the node created
            node.destroy()
            raise ex

    def check_connection(self):
        res = self._driver.list_sizes()  # try a call to the API

    def list_vm_ids(self, metadata_filter={}):
        all = self._driver.list_nodes()
        res = []
        for vm in all:

            if metadata_filter:
                vm_metadata = vm.extra['tags']
                if not metadata_filter.items() <= vm_metadata.items():
                    continue

            res.append(vm.id)
        return res

    def list_key_pairs(self, name_includes_filter=None):
        all = self._driver.list_key_pairs()

        res = []

        for kp in all:
            if name_includes_filter and not name_includes_filter in kp.name:
                continue

            res.append(kp.name)

        return res

    def list_security_groups(self, name_is_filter={}, name_includes_filter=None):
        all = self._driver.ex_list_security_groups()

        res = []

        for sg in all:

            if name_is_filter and not name_is_filter == sg:
                continue

            if name_includes_filter and not name_includes_filter in sg:
                continue

            res.append(sg)

        return res

    def destroy_key_pair(self, name):
        self.helper.destroy_keypair(self._driver, name)

    def destroy_security_group(self, id):
        self._driver.ex_delete_security_group(id)

    def destroy_vm(self, vm_or_id):
        if isinstance(vm_or_id, str):
            vm = VM(vm_or_id, None, None, None)
        else:
            vm = vm_or_id
        node = self._driver.list_nodes(ex_node_ids=[vm.id])[0]
        if node and not node.state == 'terminated':
            node.destroy()
            while not self._driver.list_nodes(ex_node_ids=[node.id])[0].state == 'terminated':
                logger.debug('VM still not deleted. Sleeping 10 seconds to wait VM is deleted')
                time.sleep(10)

        if 'keypair_name' in vm.metadata:
            self.helper.destroy_keypair(self._driver, vm.metadata['keypair_name'])

    def __get_newvm_network_param(self, network_name):
        return self.helper.get_network(self._driver, network_name)

    def __get_newvm_security_group_param(self, security_group_name):
        return self.helper.get_security_group(self._driver, security_group_name)

    def __assign_floating_ip(self, node):

        try:
            p_ip = self.__get_available_public_ip()

            if p_ip:
                logger.debug('Trying to assign the public ip %s to the new instance', p_ip)
                self._driver.ex_attach_floating_ip_to_node(node, p_ip)
                return p_ip.ip_address
            else:
                logger.error('No floating public IPs available! Cannot assign a public ip to the instance')
                raise Exception("No floating public IPs available! Cannot assign a public ip to the instance")
        except Exception as ex:
            logger.error('Got exception assigning floating ip')
            raise ex

    def __get_available_public_ip(self):
        public_ips = self._driver.ex_list_floating_ips()

        if public_ips:
            av = [i for i in public_ips if not i.node_id]
            if av:
                return av[0]
        return None

