#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import inspect
import logging
import time
from io import StringIO
from pprint import pprint
from typing import List

import paramiko
from paramiko.rsakey import RSAKey

from benchsuite.core.model.profilers import NOOPProfiler
from benchsuite.core.model.provider import Provider
from benchsuite.stdlib.providers.libcloud.aws_ec2_client import AWSEC2Client
from benchsuite.stdlib.providers.libcloud.model import AWSEC2ProviderModel, LibcloudProviderModel
from benchsuite.stdlib.providers.libcloud.openstack_client import OpenstackClient
from benchsuite.stdlib.providers.model import SSHRemoteModel
from benchsuite.stdlib.providers.libcloud.model import VM
from benchsuite.stdlib.providers.utils import ProviderTmpFileMixin, ProviderSSHTunnelingMixin
from benchsuite.stdlib.utils.ssh import run_ssh_cmd_with_client

logger = logging.getLogger(__name__)


class LibcloudProviderController(Provider, ProviderTmpFileMixin, ProviderSSHTunnelingMixin):

    PROVIDER_NAME = 'libcloud'  # optional

    def __init__(self, model):
        super().__init__(model)
        self._profiler = NOOPProfiler(None)
        self._client = None
        self.__created_key_pairs = []
        self.__created_vms: List[VM] = []

    def get_key_values_for_storage(self):
        return {'provider': self.model.name}

    def pre_process_execution_params_combination(self, orig_params):
        return dict(orig_params)

    def startup(self):
        super().startup()

        extra_args = {}

        # auht_url and ssh tunnel for the apis works only for openstack
        if hasattr(self.model, 'auth_url') and self._ssh_tunnel_local_binds:
            auth_url = f'http://{self._ssh_tunnel_local_binds["identity_endpoint"][0]}:{self._ssh_tunnel_local_binds["identity_endpoint"][1]}'
            extra_args['ex_force_base_url'] = f'http://{self._ssh_tunnel_local_binds["nova_endpoint"][0]}:{self._ssh_tunnel_local_binds["nova_endpoint"][1]}/v2.1'

        if self.model.driver == 'openstack':
            self._client = OpenstackClient(auth_url, self.model.access_id, self.model.secret_key, self.model.region, self.model.tenant, self.model.domain, self.model.auth_version, extra_args=extra_args)

        if self.model.driver == 'aws-ec2':
            self._client = AWSEC2Client(self.model.access_id, self.model.secret_key, self.model.region, extra_args=extra_args)

    def ensure_connection_to_vm(self, vm):
        if self.model.vms_conf.ssh_tunnel:
            if not 'vm-' + vm.id in self._ssh_tunnel_local_binds:
                tunnel_cfg = self.model.vms_conf.ssh_tunnel.copy()
                self._create_ssh_tunnels_from_config(
                    tunnel_cfg,
                    last_tunnel_remote_default=[SSHRemoteModel(name='vm-' + vm.id, server=vm.ip)])
            return "localhost", self._ssh_tunnel_local_binds['vm-' + vm.id][1]
        return vm.ip, vm.ssh_port

    def _get_client(self):
        if not self._client:
            self.startup()
        return self._client

    def shutdown(self):
        super().shutdown()

    def deep_cleanup(self, labels={}):
        # always use managed-by label
        metadata_filter = dict(labels)
        metadata_filter['managed-by'] = 'benchsuite'

        logger.info('Deleting all VMs with metadata %s', metadata_filter)
        ids_to_delete = self._get_client().list_vm_ids(metadata_filter=metadata_filter)
        logger.debug('Found following VMs to delete: %s', ids_to_delete)
        for id in ids_to_delete:
            self._get_client().destroy_vm(id)

        # if labels is provided, deep_cleanup should only delete resources associated with that labels.
        # Since security groups and key-pairs are not associated to any metadata we skip them
        if labels:
            return

        logger.info('Deleting all Key Pairs with name including "benchsuite_key-"')
        keypairs_to_delete = self._get_client().list_key_pairs(name_includes_filter='benchsuite_key-')
        logger.debug('Found following Key Pairs: %s', keypairs_to_delete)
        for kp in keypairs_to_delete:
            self._get_client().destroy_key_pair(kp)

        logger.info('Deleting all Security Groups with name "benchsuite_sg"')
        sg_to_delete = self._get_client().list_security_groups(name_is_filter='benchsuite_sg')
        logger.debug('Found following Security Groups: %s', sg_to_delete)
        for sg in sg_to_delete:
            self._get_client().destroy_security_group(sg)

    @staticmethod
    def build_from_dict(model):
        pass

    def check_connection(self):
        res = self._get_client().check_connection()

    def destroy_vm(self, vm):
        self._get_client().destroy_vm(vm)
        self.__created_vms.remove(vm)

    def get_vm(self, size, image):
        existing = [v for v in self.__created_vms if v.metadata.get('size') == size and v.metadata.get('image') == image]
        if not existing:
            return None
        else:
            return existing[0]

    def get_vm_ssh_client(self, vm):

        if self.model.vms_conf.ssh_tunnel:
            ip, port = self.ensure_connection_to_vm(vm)
        else:
            ip = vm.ip
            port = vm.ssh_port

        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        if vm.priv_key:
            pkey = RSAKey.from_private_key(StringIO(vm.priv_key))  # assuming it is an RSAKey
            ssh.connect(hostname=ip, port=port, username=vm.username, pkey=pkey)
        else:
            ssh.connect(hostname=ip, port=port, username=vm.username, password=vm.password)

        return ssh

    def create_vm(self, size, image, network, metadata={}):

        metadata['managed-by'] = 'benchsuite'

        vm = self._get_client().create_vm(size, image, network_name=network,
                                          metadata=metadata,
                                          assign_floating_ip=not self.model.vms_conf.no_floating_ip)
        self.__created_vms.append(vm)

        # exexute post-creation scripts. This is also used to verify that the VM is accessible
        # through ssh. If the execution fails, we destroy the VM
        self.__execute_post_create(vm, 20)

        return vm

    def __execute_post_create(self, vm, retries):
        logger.info('Trying to connect to the new instance (max_retries={0})'.format(retries))
        try:
            with self.get_vm_ssh_client(vm) as ssh_client:
                run_ssh_cmd_with_client(ssh_client, self.model.vms_conf.post_create_script.get(vm.platform, 'echo "hello world!"'))

        except Exception as ex:
            retries -= 1

            if retries > 0:
                period = 30
                logger.warning('Error connecting ({0}). The instance could be not ready yet. '
                               'Waiting {2} seconds and retry for {1} times'.format(str(ex), retries, period))
                time.sleep(int(period))
                self.__execute_post_create(vm, retries)
            else:
                logger.warning('Error connecting ({0}). Max number of retries achieved. Raising the exception'.format(str(ex)))
                raise ex

    def cleanup(self):
        for vm in self.__created_vms:
            self.destroy_vm(vm)

    def shutdown(self):
        if self._client:
            self._client = None
        self._profiler = NOOPProfiler(None)
        super().shutdown()

    @staticmethod
    def load_from_dict(model):

        # find the correct model to use based on the value of "driver" attribute
        for c in LibcloudProviderModel.__subclasses__():
            if c.__dict__['__fields__']['driver'].default == model['driver']:
                model = c.parse_obj(model)
                break

        return LibcloudProviderController(model)
