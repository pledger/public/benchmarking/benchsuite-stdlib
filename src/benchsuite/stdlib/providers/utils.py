#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import itertools
import logging
import os
import tempfile

import sshtunnel

from benchsuite.core.model.provider import ProviderMixin

logger = logging.getLogger(__name__)


class ProviderSSHTunnelingMixin(ProviderMixin):

    def _remove_ssh_tunnel(self, local_bind_name):
        tun = None
        for t in self._ssh_tunnels:
            if local_bind_name in t[1]:
                tun = t

        if tun:
            logger.info('Stopping SSH Tunnel %s', tun[1])
            tun[0].stop(force=True)
            self._ssh_tunnels.remove(tun)
            for r in tun[1]:
                del self._ssh_tunnel_local_binds[r]

    def _create_ssh_tunnel_rec(self, config, server_override=None, remote_default=None):
        if config.private_key:
            pkey = self._create_temp_file(config.private_key)
        else:
            pkey = None

        if server_override:
            ssh_server = server_override
        else:
            ssh_server = self.split_addr_port(config.server)

        if config.ssh_tunnel:
            # forward ssh to the next ssh server in the chain of ssh tunnels
            remote_servers = [self.split_addr_port(config.ssh_tunnel.server)]
            remote_names = ['ssh_forwarded_server']
        else:
            remote_binds = config.remote or remote_default
            remote_servers = [self.split_addr_port(r.server) for r in remote_binds]
            remote_names = [r.name for r in remote_binds]

        if config.port:
            local_bind = ('0.0.0.0', config.port)
        else:
            local_bind = None

        tunnel = sshtunnel.open_tunnel(
            ssh_server,
            ssh_username=config.user,
            ssh_password=config.password,
            ssh_pkey=pkey,
            remote_bind_addresses=remote_servers,
            local_bind_address=local_bind
        )

        tunnel.start()
        local_binds = {remote_names[i]: (tunnel.local_bind_hosts[i], tunnel.local_bind_ports[i]) for i in range(len(remote_names))}
        logger.info('Tunnel created ssh: %s  remote: %s  local: %s', ssh_server, remote_servers, local_binds)
        self._ssh_tunnels.append((tunnel, remote_names))
        return local_binds

    def _create_ssh_tunnels_from_config(self, config, last_tunnel_remote_default=None):

        logger.info('Creating SSH Tunnels')

        local_binds = {'ssh_forwarded_server': self.split_addr_port(config.server)}

        while config:
            remote_defaults = last_tunnel_remote_default if (not config.ssh_tunnel and not config.remote) else None
            local_binds = self._create_ssh_tunnel_rec(config, server_override=local_binds['ssh_forwarded_server'], remote_default=remote_defaults)
            if config.ssh_tunnel:
                config = config.ssh_tunnel
            else:
                config = None

        # add bindings only to the last tunnel because it is the only one used by users
        self._ssh_tunnel_local_binds.update(local_binds)

    def split_addr_port(self, addr):
        tokens = addr.split(':')
        if len(tokens) == 1:
            return tokens[0], 22
        else:
            return tokens[0], int(tokens[1])

    def initialize_mixin(self):
        self._ssh_tunnels = []
        self._ssh_tunnel_local_binds = {}

    def startup_mixin(self):
        self._ssh_tunnels = []
        self._ssh_tunnel_local_binds = {}

        # hack to avoid tunnel hanging when the .stop() is called
        # see https://github.com/pahaz/sshtunnel/issues/138
        sshtunnel.SSHTunnelForwarder.daemon_forward_servers = True
        sshtunnel.SSHTunnelForwarder.daemon_transport = True

        if self.model.ssh_tunnel:
            logger.info('Creating SSH Tunnels')
            self._create_ssh_tunnels_from_config(self.model.ssh_tunnel)

    def cleanup_mixin(self):
        self._cleanup_ssh_tunnels()

    def shutdown_mixin(self):
        self._cleanup_ssh_tunnels()

    def _cleanup_ssh_tunnels(self):
        for t in self._ssh_tunnels[::-1]:
            logger.debug('Stopping SSH tunnel %s', (t[0].local_bind_hosts, t[0].local_bind_ports))
            t[0].stop(force=True)
        self._ssh_tunnels = []
        self._ssh_tunnel_local_binds = {}


class ProviderTmpFileMixin(ProviderMixin):

    def initialize_mixin(self):
        self._temp_files = []

    def startup_mixin(self):
        self._temp_files = []

    def shutdown_mixin(self):
        self._cleanup_tmp_files()

    def _create_temp_file(self, content=""):
        handler, name = tempfile.mkstemp()
        self._temp_files.append(name)
        os.write(handler, str.encode(content))
        os.close(handler)
        logger.debug(f'temp file {name} created')
        return name

    def cleanup_mixin(self):
        self._cleanup_tmp_files()

    def _cleanup_tmp_files(self):
        for f in self._temp_files:
            os.remove(f)
            logger.debug(f'temp file {f} removed')
        self._temp_files = []

