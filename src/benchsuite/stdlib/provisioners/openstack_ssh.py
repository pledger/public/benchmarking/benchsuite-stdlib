#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from benchsuite.core.model.provider import ProviderProvisioner
from benchsuite.stdlib.execenvs.vmssh import VMSSHExecutionEnvironment
from benchsuite.stdlib.providers.libcloud.model import VM


class OpenstackSSHProvisioner(ProviderProvisioner):

    SUPPORTED_PROVIDERS = ['libcloud']
    PROVISIONED_EXEC_ENV = VMSSHExecutionEnvironment.TYPE

    def __init__(self, provider):
        super().__init__(provider)
        self.provisioned_vms = []

    def provision(self, execution_params, execution_context, hard=True, dry_run=False):

        if dry_run:
            return VMSSHExecutionEnvironment(self.provider, VM('dummy-vm', '0.0.0.0', 'dummy-user', 'dummy-platform'))

        vm = self.provider.get_vm(
            execution_params['flavour'],
            execution_params['image']
        ) if not hard else None  # try to re-use an existing vm if hard=False

        if not vm:
            metadata = {f'benchsuite.session.{k}': v for k, v in execution_context['props']['session'].items()}
            vm = self.provider.create_vm(execution_params['flavour'], execution_params['image'], network=execution_params.get('network', None), metadata=metadata)

        self.provisioned_vms = [vm]

        return VMSSHExecutionEnvironment(self.provider, vm, tags={
            'cpu': vm.metadata['cpu'],
            'memory': vm.metadata['memory'],
            'disk': vm.metadata['disk'],
            'flavour': vm.metadata['flavour'],
            'image': vm.metadata['image']
        }, bsid_tags=['flavour', 'image'])

    def ensure_connectivity(self, exec_env):
        # nothing because it is in the VMSSHExecutionEnvironment
        pass

    def deprovision(self, hard=True):
        if not self.provisioned_vms:
            return
        vm = self.provisioned_vms[0]

        if hard:
            self.provider.destroy_vm(vm)

        self.provisioned_vms = []

