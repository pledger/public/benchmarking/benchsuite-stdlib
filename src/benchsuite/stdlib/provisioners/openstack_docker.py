#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import logging

from benchsuite.core.model.provider import ProviderProvisioner
from benchsuite.stdlib.execenvs.docker import DockerExecutionEnvironment
from benchsuite.stdlib.providers.docker.controller import DockerProviderController
from benchsuite.stdlib.providers.docker.model import DockerProviderModel
from benchsuite.stdlib.providers.model import ProviderSSHTunnelingModel, SSHRemoteModel
from benchsuite.stdlib.utils.ssh import run_ssh_cmd_with_client

logger = logging.getLogger(__name__)


class OpenstackDockerProvisioner(ProviderProvisioner):

    SUPPORTED_PROVIDERS = ['libcloud']
    PROVISIONED_EXEC_ENV = DockerExecutionEnvironment.TYPE

    DEBIAN_DOCKER_INSTALL = '''sudo DEBIAN_FRONTEND=noninteractive apt-get -yq update
sudo mkdir -p /etc/systemd/system/docker.service.d/
sudo tee /etc/systemd/system/docker.service.d/override.conf <<EOF
[Service]
ExecStart=
ExecStart=/usr/sbin/dockerd
EOF
sudo mkdir -p /etc/docker/
sudo tee /etc/docker/daemon.json <<EOF
{
  "hosts": ["unix:///var/run/docker.sock", "tcp://127.0.0.1:2375"]
}
EOF
sudo DEBIAN_FRONTEND=noninteractive apt-get -yq install docker.io
'''

    def __init__(self, provider_controller):
        super().__init__(provider_controller)
        self.provisioned_vms = []

    def provision(self, execution_params, execution_context, hard=True, dry_run=False):
        vm = self.provider.get_vm(
            execution_params['flavour'],
            execution_params['image']
        ) if not hard else None  # try to re-use an existing vm if hard=False

        if dry_run:
            return DockerExecutionEnvironment(None, {}, [])

        if not vm:
            metadata = {f'benchsuite.session.{k}': v for k, v in execution_context['props']['session'].items()}
            vm = self.provider.create_vm(execution_params['flavour'], execution_params['image'], network=execution_params.get('network', None), metadata=metadata)

        self.provisioned_vms = [vm]

        if not vm.metadata.get('docker_installed'):
            self._install_docker(vm)

        if f'{vm.id}-docker' not in self.provider._ssh_tunnel_local_binds:
            ip, port = self.provider.ensure_connection_to_vm(vm)

            self.provider._create_ssh_tunnels_from_config(ProviderSSHTunnelingModel(
                server=f'{ip}:{port}',
                user=vm.username,
                private_key=vm.priv_key,
                remote=[SSHRemoteModel(name=f'{vm.id}-docker', server='127.0.0.1:2375')]))

        docker_port = self.provider._ssh_tunnel_local_binds[f'{vm.id}-docker']
        logger.info('Created SSH tunnel to access docker daemon %s', docker_port)

        model = DockerProviderModel(id='docker-engine', name='docker-engine', profiler='docker-stats', base_url=f'tcp://{docker_port[0]}:{docker_port[1]}')
        prov = DockerProviderController(model)

        return DockerExecutionEnvironment(prov, tags={
            'cpu': vm.metadata['cpu'],
            'memory': vm.metadata['memory'],
            'disk': vm.metadata['disk'],
            'flavour': vm.metadata['flavour'],
            'image': vm.metadata['image']
        }, bsid_tags=['flavour', 'image'])

    def ensure_connectivity(self, exec_env):

        vm = self.provisioned_vms[0]

        if f'{vm.id}-docker' not in self.provider._ssh_tunnel_local_binds:
            ip, port = self.provider.ensure_connection_to_vm(vm)

            self.provider._create_ssh_tunnels_from_config(ProviderSSHTunnelingModel(
                server=f'{ip}:{port}',
                user=vm.username,
                private_key=vm.priv_key,
                remote=[SSHRemoteModel(name=f'{vm.id}-docker', server='127.0.0.1:2375')]))

            docker_port = self.provider._ssh_tunnel_local_binds[f'{vm.id}-docker']
            logger.info('Created SSH tunnel to access docker daemon %s', docker_port)

            exec_env._client.model.base_url = f'tcp://{docker_port[0]}:{docker_port[1]}'
            exec_env._client.startup()

    def deprovision(self, hard=True):
        if not self.provisioned_vms:
            return

        vm = self.provisioned_vms[0]

        if f'{vm.id}-docker' in self.provider._ssh_tunnel_local_binds:
            self.provider._remove_ssh_tunnel(f'{vm.id}-docker')

        if hard:
            self.provider.destroy_vm(vm)

    def __get_docker_install_cmd(self, vm_platform):
        if vm_platform.startswith('ubuntu'):
            return OpenstackDockerProvisioner.DEBIAN_DOCKER_INSTALL.replace("/usr/sbin/dockerd", "/usr/bin/dockerd")

        return OpenstackDockerProvisioner.DEBIAN_DOCKER_INSTALL

    def _install_docker(self, vm):
        logger.info('Installing docker engine in vm %s', vm.id)
        with self.provider.get_vm_ssh_client(vm) as ssh_client:
            status, out, err = run_ssh_cmd_with_client(ssh_client, self.__get_docker_install_cmd(vm.platform))
            if status == 0 and not err:
                vm.metadata['docker_installed'] = True
            else:
                logger.error('Docker installation failed: status: %s\noutput: %s\nerror: %s', status, out, err)
                raise Exception('Error installing docker: %s', err)

