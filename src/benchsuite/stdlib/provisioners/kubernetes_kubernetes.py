#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from benchsuite.core.model.provider import ProviderProvisioner
from benchsuite.stdlib.execenvs.kubernetes import KubernetesExecutionEnvironment


class KubernetesKubernetesProvisioner(ProviderProvisioner):

    SUPPORTED_PROVIDERS = ['kubernetes']
    PROVISIONED_EXEC_ENV = 'kubernetes'

    def __init__(self, provider_controller):
        super().__init__(provider_controller)

    def provision(self, execution_params, execution_context, hard=True, dry_run=False):
        return KubernetesExecutionEnvironment(self.provider, tags={
            'namespace': execution_params['namespace']
        }, bsid_tags=['namespace', 'node'])  # node will be added by the executor

    def deprovision(self, hard=True):
        pass

    def ensure_connectivity(self, exec_env):
        self.provider.startup()