#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
from benchsuite.core.model.provider import ExecutionEnvironment
from benchsuite.stdlib.utils.ssh import run_ssh_cmd_with_client


class VMSSHExecutionEnvironment(ExecutionEnvironment):

    TYPE = 'ssh'

    def __init__(self, provider, vm, tags, bsid_tags):
        super().__init__(tags=tags, bsid_tags=bsid_tags)
        self.provider = provider
        self.vm = vm

    def execute_cmd(self, cmd, env_dict={}):
        with self.provider.get_vm_ssh_client(self.vm) as ssh_client:
            return run_ssh_cmd_with_client(ssh_client, cmd, env_dict=env_dict)
