#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

from benchsuite.core.db.configuration_db import BenchsuiteConfigurationDB
from benchsuite.core.loaders.workload_loaders import WorkloadLoader
from benchsuite.core.model.benchmark import load_workloads_from_config_file

logger = logging.getLogger(__name__)


class FileWorkloadLoader(WorkloadLoader):

    def load(self):
        return load_workloads_from_config_file(self.workload_string, self.qualifier)


class DBWorkloadLoader(WorkloadLoader):

    def load(self):
        bsdb = BenchsuiteConfigurationDB(**self.controller_config.flatten_dict())
        return [bsdb.get_workload(self.workload_string, resolve=True)]

