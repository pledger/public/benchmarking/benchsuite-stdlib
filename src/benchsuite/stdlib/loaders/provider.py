#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging

from benchsuite.core.db.configuration_db import BenchsuiteConfigurationDB
from benchsuite.core.loaders.provider_loaders import ProviderLoader
from benchsuite.core.model.provider import load_provider_from_dict, load_service_provider_from_config_file

logger = logging.getLogger(__name__)


class FileProviderLoader(ProviderLoader):

    def load(self):
        return load_service_provider_from_config_file(self.provider_string, self.qualifier, attrs_overrides=self.attrs_overrides)


class DBProviderLoader(ProviderLoader):

    def load(self):
        bsdb = BenchsuiteConfigurationDB(**self.controller_config.flatten_dict())
        p = bsdb.get_provider(self.provider_string, show_secrets=True)
        return load_provider_from_dict(p, attrs_overrides=self.attrs_overrides)

