#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import re

from benchsuite.core.model.execution import ExecutionResultParser


class SysbenchCPUParser(ExecutionResultParser):

    start_pattern = re.compile("^###([a-zA-Z]+)###([0-9]+)###$")

    cpu_metrics_patterns = {
        'events_rate': (re.compile("^.*events per second:(.*)$"), 'num/s'),
        'total_time':  (re.compile("^.*total time:(.*)s$"), 's'),
        'latency_min': (re.compile("^.*min:(.*)$"), 'ms'),
        'latency_max': (re.compile("^.*max:(.*)$"), 'ms'),
        'latency_avg': (re.compile("^.*avg:(.*)$"), 'ms'),
        'latency_95':  (re.compile("^.*95th percentile:(.*)$"), 'ms')
    }

    def get_metrics(self, stdout, stderr, execution=None):
        """
        https://wiki.gentoo.org/wiki/Sysbench
        """
        lines = stdout.split('\n')

        # detect the different tool executions
        execs_out = []
        istart = iend = 0
        while istart < len(lines):
            match = self.start_pattern.match(lines[istart])
            if match:
                iend = istart
                while iend < len(lines):
                    if lines[iend] == '###END###':
                        execs_out.append((match.group(1), int(match.group(2)), istart, iend))
                        istart = iend
                        break
                    iend+=1
            istart+=1

        res = {}
        for exec in execs_out:
            if exec[0] == 'cpu':
                res.update(self.get_cpu_metrics(lines[exec[2]:exec[3]+1], exec[1]))

        return res

    def get_cpu_metrics(self, output, n_threads):
        res = {}

        for l in output:
            for k,v in self.cpu_metrics_patterns.items():
                pattern, unit = v
                m = pattern.match(l)
                if(m):
                    res['{0}_{1}'.format(k, n_threads)] = {'value':float(m.group(1).strip()), 'unit': unit}

        return res
