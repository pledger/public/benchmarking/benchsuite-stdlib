#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import json

from benchsuite.core.model.execution import ExecutionResultParser


class IPerfParser(ExecutionResultParser):

    def get_metrics(self, stdout, stderr, execution=None):

        json_out = stdout.replace("\'", "\"").replace("False", "\"False\"").replace("True", "\"True\"")
        data = json.loads(json_out)
        if data:
            return self.__get_json_tcp_metrics(data)
        return {}

    def __get_json_tcp_metrics(self, data):
        res = {}

        res['received_sum'] = {'value': data['end']['sum_received']['bytes'], 'unit': 'bytes'}
        res['sent_sum'] = {'value': data['end']['sum_sent']['bytes'], 'unit': 'bytes'}
        res['received_bandwidth_sum'] = {'value': data['end']['sum_received']['bits_per_second'], 'unit': 'bit/s'}
        res['sent_bandwidth_sum'] = {'value': data['end']['sum_sent']['bits_per_second'], 'unit': 'bit/s'}

        return res

    ## OLD STUFF ##

    def __get_tcp_metrics(self, stdout):

        res = {}
        lines = stdout.split("\n")
        data = [l.split(',') for l in lines if len(l.split(',')) == 9]

        c = 1
        for i in range(0, len(data) -1) if len(data) > 1 else range(0,1):
            res['transferred_'+str(c)] = {'value': int(data[i][7]), 'unit': 'bytes'}
            res['bandwidth_'+str(c)] = {'value': int(data[i][8]), 'unit': 'bit/s'}
            c+=1

        if len(data) > 1:
            res['transferred_sum'] =  {'value': int(data[c-1][7]), 'unit': 'bytes'}
            res['bandwidth_sum'] =    {'value': int(data[c-1][8]), 'unit': 'bit/s'}

        return res

    def __get_udp_metrics(self, stdout):

        res = {}
        lines = stdout.split("\n")
        data = [l.split(',') for l in lines if len(l.split(',')) == 14]

        c = 1
        for i in data:
            res['transferred_'+str(c)] = {'value': int(i[7]), 'unit': 'bytes'}
            res['bandwidth_'+str(c)] =    {'value': int(i[8]), 'unit': 'bit/s'}
            res['total_datagrams_'+str(c)] = {'value': int(i[11]), 'unit': 'num'}
            res['lost_datagrams'+str(c)] = {'value': int(i[10]), 'unit': 'num'}
            res['jitter_'+str(c)] = {'value': i[9], 'unit': 'ms'}
            res['outoforder_'+str(c)] = {'value': int(i[13]), 'unit': 'num'}
            c+=1

        tavg = sum([int(x[7]) for x in data]) / len(data)
        bavg = sum([int(x[8]) for x in data]) / len(data)
        davg = sum([int(x[11]) for x in data]) / len(data)
        lavg = sum([int(x[10]) for x in data]) / len(data)
        javg = sum([float(x[9]) for x in data]) / len(data)
        oavg = sum([int(x[13]) for x in data]) / len(data)
        res['transferred_avg'] = {'value': tavg, 'unit': 'bytes'}
        res['bandwidth_avg'] =  {'value': bavg, 'unit': 'bit/s'}
        res['total_datagrams_avg'] =  {'value': davg, 'unit': 'num'}
        res['lost_datagrams_avg'] =  {'value': lavg, 'unit': 'num'}
        res['jitter_avg'] =  {'value': javg, 'unit': 'ms'}
        res['outoforder_avg'] =  {'value': oavg, 'unit': 'num'}

        return res