#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
import json
import logging
import re
import jsonpath_ng
from pprint import pprint

from benchsuite.core.model.execution import ExecutionResultParser

logger = logging.getLogger(__name__)


class CommonRegexParser(ExecutionResultParser):

    metric_value_converters = {
        'int': int,
        'float': float
    }

    def json_based_metrics(self, definition, stream):

        res = {}
        jdoc = json.loads(stream)

        for m in definition:
            jsonpath_expr = jsonpath_ng.parse(m['path'])
            raw_value = [str(match.value) for match in jsonpath_expr.find(jdoc)]
            m_value = self.metric_value_converters[m.get('type', 'float')](raw_value[0])
            res[m['name']] = {'value': m_value, 'unit': m.get('unit', None)}

        return res

    def regex_based_metrics(self, definition, stream):

        res = {}

        regex_orig = definition

        definitions = []
        for m in re.finditer(r'\[\[.+?\]\]', regex_orig):
            d = regex_orig[m.start() + 2:m.end() - 2]
            tokens = d.split(':', maxsplit=2)
            name = tokens[0]
            unit = None
            type = 'float'
            if len(tokens) > 1:
                unit = tokens[1] or None
            if len(tokens) > 2:
                type = tokens[2] or 'float'
            definitions.append({'name': name, 'unit': unit, 'type': type})

        regex_str = re.escape(regex_orig)

        # make regex more flexible on number of consecutive new lines and spaces
        regex_str = re.sub(r'(\\\n)+', '\\\n\\\s', regex_str)
        regex_str = re.sub(r'\\\ (\\\ )+', '\\\s+', regex_str)

        regex_pattern = re.sub(r'\\\[\\\[.+?\\\]\\\]', '(.*)', regex_str)
        regex = re.compile(regex_pattern)
        logger.info('Regex generated: %s', regex.pattern)

        match = re.search(regex, stream)

        logger.info('Metrics definitions: %s', definitions)

        if not match:
            logger.warning('match not found for regex %s', definition)
            return res

        for idx in range(match.lastindex):
            m_token = match.group(idx + 1).strip()
            metric_descr = definitions[idx]
            if 'type' in metric_descr:
                m_value = self.metric_value_converters[metric_descr['type']](m_token)
            else:
                m_value = float(m_token)

            res[metric_descr['name']] = {'value': m_value, 'unit': metric_descr.get('unit', None)}

        return res

    def get_metrics(self, stdout, stderr, execution=None):

        res = {}

        for metric_block in self.metrics_description:

            stream = stdout
            if 'stream' in metric_block and metric_block['stream'] == 'stderr':
                stream = stderr

            if 'line-selector' in metric_block:
                lines = stream.strip().split('\n')
                if metric_block['line-selector'] == 'last':
                    stream = lines[-1]
                else:  # treat as a regex
                    regex = re.compile(metric_block['line-selector'])
                    print(stream.replace('\n', '\\\\n'))
                    match = regex.search(stream)
                    if not match:
                        logger.warning('line selector did not return any match!')
                        stream = ''
                    else:
                        stream = match.group(0)

            if 'regex' in metric_block:
                res.update(self.regex_based_metrics(metric_block['regex'], stream))

            if 'json' in metric_block:
                res.update(self.json_based_metrics(metric_block['json'], stream))

        return res


class GenericRegexParser(CommonRegexParser):

    def __init__(self, config=None, **kwargs):
        if config:
            self.metrics_description = config.get('metric-definitions', [])
        else:
            logger.fatal('GenericRegexParser needs a config to be initialized')