#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import math
import re

from benchsuite.core.model.execution import ExecutionResultParser

logger = logging.getLogger(__name__)


class YCSBParser(ExecutionResultParser):

    def get_metrics(self, stdout, stderr, execution=None):
        #
        # Metrics in the YCSB outputs appears in this format:
        # [<operation_type>], <metrics>, <value>
        # e.g.
        # [INSERT], MaxLatency(us), 14423.0
        # [SCAN], Operations, 4748.0

        lines = stdout.split('\n')

        # filter-out all the non-metrics lines
        metrics_lines = [l for l in lines if l.startswith('[')]

        # parse all the metrics from the output
        parsed_metrics = {}
        for l in metrics_lines:
            m = re.search('^\[(.+)\],\ (.+),\ (.+)$', l)
            op_type = m.group(1).lower()
            op_met = m.group(2)
            met_val = m.group(3)
            try:
                fval = float(met_val)
                if math.isnan(fval):
                   continue
            except:
                logger.error('output line discarded because the value is not convertible in a float: %s', l)
                continue
            if op_type not in parsed_metrics:
                parsed_metrics[op_type] = {}

            parsed_metrics[op_type][op_met] = met_val

        # keep only interesting metrics, do some sanitization of names
        out = {
            'ops_throughput': {'value': float(parsed_metrics['overall']['Throughput(ops/sec)']), 'unit': 'ops/s'},
            'runtime': {'value': float(parsed_metrics['overall']['RunTime(ms)']), 'unit': 'ms'}
        }

        out.update(self.__get_metrics_by_operation_type(parsed_metrics, 'insert'))
        out.update(self.__get_metrics_by_operation_type(parsed_metrics, 'read'))
        out.update(self.__get_metrics_by_operation_type(parsed_metrics, 'update'))
        out.update(self.__get_metrics_by_operation_type(parsed_metrics, 'scan'))

        return out

    def __get_metrics_by_operation_type(self, parsed_metrics, op_type):

        if op_type not in parsed_metrics:
            return {}

        return {
            op_type+'_ops': {'value': int(float(parsed_metrics[op_type]['Operations'])), 'unit': 'num'},
            op_type+'_latency_avg': {'value': float(parsed_metrics[op_type]['AverageLatency(us)']), 'unit': 'us'},
            op_type+'_latency_min': {'value': float(parsed_metrics[op_type]['MinLatency(us)']), 'unit': 'us'},
            op_type+'_latency_max': {'value': float(parsed_metrics[op_type]['MaxLatency(us)']), 'unit': 'us'},
            op_type+'_latency_95': {'value': float(parsed_metrics[op_type]['95thPercentileLatency(us)']), 'unit': 'us'},
            op_type+'_latency_99': {'value': float(parsed_metrics[op_type]['99thPercentileLatency(us)']), 'unit': 'us'}
        }
