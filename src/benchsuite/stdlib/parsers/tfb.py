#  Benchmarking Suite
#  Copyright 2014-2022 Engineering Ingegneria Informatica S.p.A.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

import logging
import re
from benchsuite.core.model.execution import ExecutionResultParser

logger = logging.getLogger(__name__)

import re
import subprocess

def get_ms(time_str):
    '''
    source https://github.com/MaartenSmeets/db_perftest/blob/master/test_scripts/wrk_parser.py

    :param time_str:
    :return:
    '''
    x = re.search("^(\d+\.*\d*)(\w*)$", time_str)
    if x is not None:
        size = float(x.group(1))
        suffix = (x.group(2)).lower()
    else:
        return time_str

    if suffix == 'us':
        return size / 1000
    elif suffix == 'ms':
        return size
    elif suffix == 's':
        return size * 1000
    elif suffix == 'm':
        return size * 1000 * 60
    elif suffix == 'h':
        return size * 1000 * 60 * 60
    else:
        return size

    return False


class TFBParser(ExecutionResultParser):

    def get_metrics(self, stdout, stderr, execution=None):
        '''
        source https://github.com/TechEmpower/FrameworkBenchmarks/blob/2cebf97e4ede1b928bb9bcd1782d23e4368d2df9/toolset/utils/results.py
        :param tool:
        :param workload:
        :param logs:
        :return:
        '''

        results = dict()
        results['results'] = []
        stats = []
        is_warmup = True
        rawData = None
        for line in stdout.splitlines():
            if "Queries:" in line or "Concurrency:" in line:
                is_warmup = False
                rawData = None
                continue
            if "Warmup" in line or "Primer" in line:
                is_warmup = True
                continue
            if not is_warmup:
                if rawData is None:
                    rawData = dict()
                    results['results'].append(rawData)
                if "Latency" in line:
                    m = re.findall(r"([0-9]+\.*[0-9]*[us|ms|s|m|%]+)",
                                   line)
                    if len(m) == 4:
                        rawData['latencyAvg'] = get_ms(m[0])
                        rawData['latencyStdev'] = get_ms(m[1])
                        rawData['latencyMax'] = get_ms(m[2])
                if "Req/Sec" in line:
                    m = re.findall(r"([0-9]+\.*[0-9]*[%]*)",
                                   line)
                    if len(m) == 4:
                        rawData['reqsecAvg'] = get_ms(m[0])
                        rawData['reqsecStdev'] = get_ms(m[1])
                        rawData['reqsecMax'] = get_ms(m[2])
                if "requests in" in line:
                    m = re.search("([0-9]+) requests in", line)
                    if m is not None:
                        rawData['totalRequests'] = int(m.group(1))
                if "Socket errors" in line:
                    if "connect" in line:
                        m = re.search("connect ([0-9]+)", line)
                        rawData['connect'] = int(m.group(1))
                    if "read" in line:
                        m = re.search("read ([0-9]+)", line)
                        rawData['read'] = int(m.group(1))
                    if "write" in line:
                        m = re.search("write ([0-9]+)", line)
                        rawData['write'] = int(m.group(1))
                    if "timeout" in line:
                        m = re.search("timeout ([0-9]+)", line)
                        rawData['timeout'] = int(m.group(1))
                if "Non-2xx" in line:
                    m = re.search("Non-2xx or 3xx responses: ([0-9]+)",
                                  line)
                    if m != None:
                        rawData['5xx'] = int(m.group(1))
                if "STARTTIME" in line:
                    m = re.search("[0-9]+", line)
                    rawData["startTime"] = int(m.group(0))
                if "ENDTIME" in line:
                    m = re.search("[0-9]+", line)
                    rawData["endTime"] = int(m.group(0))
                    #test_stats = self.__parse_stats(
                    #    framework_test, test_type,
                    #    rawData["startTime"], rawData["endTime"], 1)
                    #stats.append(test_stats)

        res = {
            'duration': {'value': rawData['endTime'] - rawData['startTime'] , 'unit': 's'},
            'latency_avg': {'value': rawData['latencyAvg'], 'unit': 'ms'},
            'latency_stdev': {'value': rawData['latencyStdev'], 'unit': 'ms'},
            'latency_max': {'value': rawData['latencyMax'], 'unit': 'ms'},
            'total_requests': {'value': rawData['totalRequests'], 'unit': None},
            'reqsec_avg': {'value': rawData['reqsecAvg'], 'unit': None},
            'reqsec_stdev': {'value': rawData['reqsecStdev'], 'unit': None},
            'reqsec_max': {'value': rawData['reqsecMax'], 'unit': None},
        }

        return res